﻿using System.Windows.Navigation;
using InvoicingTool.UI.Views;

namespace InvoicingTool.UI
{
    public class Navigator
    {
        private static Preparation _preparationView;
        private static Processing _processingView;
        private static Confirmation _confirmationView;

        public static Preparation PreparationView
        {
            get { return _preparationView ?? (_preparationView = new Preparation()); }
        }

        public static Processing ProcessingView
        {
            get { return _processingView ?? (_processingView = new Processing()); }
        }

        public static Confirmation ConfirmationView
        {
            get { return _confirmationView ?? (_confirmationView = new Confirmation()); }
        }

        public static NavigationService NavigationService { get; set; }
    }
}
