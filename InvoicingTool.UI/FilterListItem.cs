﻿using System.Collections.Generic;
using Telerik.Windows.Data;

namespace InvoicingTool.UI
{
    public class FilterListItem
    {
        public string FilterName { get; set; }
        public IList<FilterDescriptor> FilterDescriptors { get; set; }
    }
}