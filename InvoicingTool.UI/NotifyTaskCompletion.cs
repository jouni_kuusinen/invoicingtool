﻿using System;
using System.ComponentModel;
using System.Threading;
using System.Threading.Tasks;

namespace InvoicingTool.UI
{
    public sealed class NotifyTaskCompletion<TResult> : INotifyPropertyChanged
    {
        public Task<TResult> Task { get; private set; }
        public NotifyTaskCompletion(Func<CancellationToken, Task<TResult>> task, CancellationToken token)
        {

            if (Task == null)
            {
                TaskCompletion = WatchTask(task, token);
            }

        }


        public Task TaskCompletion { get; set; }


        private async Task WatchTask(Func<CancellationToken, Task<TResult>> task, CancellationToken token)
        {
            try
            {
                Task = task(token);
                await Task;
            }
            catch
            {
                // ignored
            }

            OnTaskCompletion();
        }

        private void OnTaskCompletion()
        {
            var propertyChanged = PropertyChanged;
            if (propertyChanged == null)
                return;
            propertyChanged(this, new PropertyChangedEventArgs("Status"));
            propertyChanged(this, new PropertyChangedEventArgs("IsCompleted"));
            propertyChanged(this, new PropertyChangedEventArgs("IsNotCompleted"));
            if (Task.IsCanceled)
            {
                propertyChanged(this, new PropertyChangedEventArgs("IsCanceled"));
            }
            else if (Task.IsFaulted)
            {
                propertyChanged(this, new PropertyChangedEventArgs("IsFaulted"));
                propertyChanged(this, new PropertyChangedEventArgs("Exception"));
                propertyChanged(this,
                  new PropertyChangedEventArgs("InnerException"));
                propertyChanged(this, new PropertyChangedEventArgs("ErrorMessage"));
            }
            else
            {
                propertyChanged(this,
                  new PropertyChangedEventArgs("IsSuccessfullyCompleted"));
                propertyChanged(this, new PropertyChangedEventArgs("Result"));
            }
        }
        public TaskStatus Status { get { return Task.Status; } }
        public bool IsCompleted { get { return Task.IsCompleted; } }
        public bool IsNotCompleted { get { return !Task.IsCompleted; } }
        public bool IsSuccessfullyCompleted
        {
            get
            {
                return Task.Status ==
                    TaskStatus.RanToCompletion;
            }
        }
        public bool IsCanceled { get { return Task.IsCanceled; } }
        public bool IsFaulted { get { return Task.IsFaulted; } }
        public AggregateException Exception { get { return Task.Exception; } }
        public Exception InnerException
        {
            get
            {
                return (Exception == null) ?
                    null : Exception.InnerException;
            }
        }
        public string ErrorMessage
        {
            get
            {
                return (InnerException == null) ?
                    null : InnerException.Message;
            }
        }

        public TResult Result
        {
            get
            {
                return Task.Status == TaskStatus.RanToCompletion ? Task.Result : default(TResult);
            }

        }

        public event PropertyChangedEventHandler PropertyChanged;

    }
}
