﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace InvoicingTool.UI
{
    public static class StringExtensions
    {
        public static DateTime? ParseAsDate(this string date)
        {
            DateTime? dateTime = null;

            int isNumber;

            //1) Determine if no separators were specified
            if (int.TryParse(date, NumberStyles.Integer, CultureInfo.CurrentCulture, out isNumber)) //ie: If the user typed only numerics, leaving out any separators
            {
                //2) Determine if the length without separators is correct                               
                var longestDate = new DateTime(1977, 12, 20).ToShortDateString();
                var longestDateFormatWithoutSeparator = new string(longestDate.Where(char.IsDigit).ToArray());
                if (date.Length == longestDateFormatWithoutSeparator.Length) //NOTE: Some cultures write "DD" "01" as "1" - force them to type "01"
                {
                    //3) Determine what the separator is
                    char separator = longestDate.First(ch => !char.IsDigit(ch));

                    //4) Insert the separator into the date string
                    var separatorIndexes = new List<int>();
                    int currentIndex = longestDate.IndexOf(separator, 0, longestDate.Length);
                    while (currentIndex > 0)
                    {
                        separatorIndexes.Add(currentIndex);
                        currentIndex = longestDate.IndexOf(separator, currentIndex + 1, longestDate.Length - currentIndex - 1);
                    }

                    date = separatorIndexes.Aggregate(date, (current, separatorIndex) => current.Insert(separatorIndex, new string(separator, 1)));

                    //5) Parse the new date string to check validity
                    DateTime parseDate;
                    if (DateTime.TryParse(date, CultureInfo.CurrentCulture, DateTimeStyles.AssumeLocal, out parseDate))
                    {
                        dateTime = parseDate;
                    }
                }
            }

            return dateTime;
        }
    }
}
