﻿using System;
using System.Windows.Data;
using System.Windows.Media.Imaging;

namespace InvoicingTool.UI.Converters
{
    public class NoteToImageConverter : IValueConverter
    {

        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            BitmapImage image;

            string Note = (string)value;
            if (Note.Length > 0)
                image = new BitmapImage(new Uri("/InvoicingTool.UI;component/Assets/note.png", UriKind.Relative));
            else
                image = null;

            return image;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
