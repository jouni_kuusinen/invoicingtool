﻿using System;
using System.Windows.Data;

namespace InvoicingTool.UI.Converters
{
    public class NotificationTypeToColor : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null)
                return new ArgumentNullException("value", "Notification type cannot be null");
            switch ((NotificationType)value)
            {
                case NotificationType.Success:
                    return "#FF5CB85C";
                case NotificationType.Warning:
                    return "#FFF0AD4E";
                case NotificationType.Error:
                    return "#FFD9534F";
            }
            return new InvalidOperationException("Notification type not found or color convertion failed.");
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
