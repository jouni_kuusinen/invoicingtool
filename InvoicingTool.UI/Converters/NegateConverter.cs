﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace InvoicingTool.UI.Converters
{
    public class NegateConverter : IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return !(value as bool?);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return !(value as bool?);
        }

    }
}
