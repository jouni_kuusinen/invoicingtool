﻿using System;
using System.Globalization;
using System.Threading.Tasks;
using System.Windows.Data;

namespace InvoicingTool.UI.Converters
{
    public sealed class TaskStatusToBool : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var val = value as TaskStatus?;
            if (val != null)
            {
                if (val.Value == TaskStatus.Faulted)
                    return false;

                if (val.Value == TaskStatus.RanToCompletion)
                    return false;
            }

            return true;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
