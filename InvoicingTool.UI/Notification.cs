﻿namespace InvoicingTool.UI
{
    public class Notification
    {
        public int Id { get; set; }
        public NotificationType Type { get; set; }
        public string Text { get; set; }
    }

    public enum NotificationType
    {
        Success,
        Warning,
        Error
    }
}
