﻿using Telerik.Windows.Controls;
using Telerik.Windows.Data;
using Telerik.Windows.Persistence.Services;

namespace InvoicingTool.UI
{
    public class FilterDescriptorCustomPropertyProvider : ICustomPropertyProvider
    {
        public CustomPropertyInfo[] GetCustomProperties()
        {
            return new CustomPropertyInfo[]
            {
                new CustomPropertyInfo("LogicalOperator", typeof(FilterCompositionLogicalOperator)),
                new CustomPropertyInfo("FilterDescriptors", typeof(CompositeFilterDescriptorCollection)),
            };
        }

        public void InitializeObject(object context)
        {
            RadDataFilter filter = context as RadDataFilter;
            if (filter != null) filter.FilterDescriptors.Clear();
        }

        public object InitializeValue(CustomPropertyInfo customPropertyInfo, object context)
        {
            return null;
        }

        public object ProvideValue(CustomPropertyInfo customPropertyInfo, object context)
        {
            RadDataFilter filter = context as RadDataFilter;
            switch (customPropertyInfo.Name)
            {
                case "LogicalOperator":
                    if (filter != null)
                        return filter.ViewModel.CompositeFilter.LogicalOperator;
                    break;
                case "FilterDescriptors":
                    if (filter != null) 
                        return filter.FilterDescriptors;
                    break;
            }
            return null;
        }

        public void RestoreValue(CustomPropertyInfo customPropertyInfo, object context, object value)
        {
            RadDataFilter filter = context as RadDataFilter;

            if (customPropertyInfo.Name == "LogicalOperator")
            {
                filter.ViewModel.CompositeFilter.LogicalOperator = (FilterCompositionLogicalOperator)value;
            }
            if (customPropertyInfo.Name == "FilterDescriptors")
            {
                CompositeFilterDescriptorCollection filters = value as CompositeFilterDescriptorCollection;
                if (filter != null)
                {
                    foreach (var item in filters)
                    {
                        filter.FilterDescriptors.Add(item);
                    }
                }
            }
        }
    }
}
