﻿using System.Linq;
using InvoicingTool.UI.ViewModels;
using Telerik.Windows.Data;

namespace InvoicingTool.UI
{
    public sealed class GrossPriceSumFunction : AggregateFunction<OrderRowViewModel, decimal>
    {
        public GrossPriceSumFunction()
        {
            Caption = "Gross Total: ";
            ResultFormatString = "{0:### ### ### ##0.00}";
            //AggregationExpression = models => models.Sum(p => p.GrossPrice + p.Airmail + p.Postage);
            AggregationExpression = models => models.Sum(p => p.GrossPrice);
        }
    }
}
