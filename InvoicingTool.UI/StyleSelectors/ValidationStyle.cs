﻿using System.Windows;
using System.Windows.Controls;
using InvoicingTool.UI.ViewModels;

namespace InvoicingTool.UI.StyleSelectors
{
    public class ValidationStyle : StyleSelector
    {
        public override Style SelectStyle(object item, DependencyObject container)
        {
            if (item is OrderRowViewModel)
            {
                var order = item as OrderRowViewModel;

                if (order.Sent.HasValue && order.Sent.Value)
                    return SentStyle;

                if (order.EContent)
                    return EContentStyle;

                if (order.CustomizedPrice)
                    return CustomizedPriceStyle;

                if (order.ExceptionalPeriod)
                    return ExceptionalPeriodStyle;

                if (order.NegativeMargin)
                    return NegativeMarginStyle;

                if (order.ModifiedPrice)
                    return ModifiedPriceStyle;

                return NormalRowStyle;
            }

            return base.SelectStyle(item, container);
        }

        public Style SentStyle { get; set; }
        public Style NegativeMarginStyle { get; set; }
        public Style ExceptionalPeriodStyle { get; set; }
        public Style CustomizedPriceStyle { get; set; }
        public Style ModifiedPriceStyle { get; set; }
        public Style NormalRowStyle { get; set; }
        public Style EContentStyle { get; set; }
    }
}
