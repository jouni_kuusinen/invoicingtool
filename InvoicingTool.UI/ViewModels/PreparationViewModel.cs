﻿using GalaSoft.MvvmLight.CommandWpf;
using InvoicingTool.Business;
using InvoicingTool.Entities;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using Telerik.Windows.Controls;
using ViewModelBase = InvoicingTool.UI.Helpers.ViewModelBase;

namespace InvoicingTool.UI.ViewModels
{
    public sealed class PreparationViewModel : ViewModelBase
    {
        private ObservableCollection<CustomerClass> customerClasses;
        private ObservableCollection<CustomerGroup> customerGroups;
        private ObservableCollection<CustomerShipViewModel> customerShips;
        private DateTime? endDate;
        private ObservableCollection<InvoicingGroup> invoicingGroups;
        private ObservableCollection<ProformaInvoice> proformaInvoices;
        private CustomerClass selectedCustomerClass;
        private CustomerGroup selectedCustomerGroup;
        private CustomerShipViewModel selectedCustomership;
        private InvoicingGroup selectedInvoicingGroupDomestic;
        private InvoicingGroup selectedInvoicingGroupForeign;
        private ProformaInvoice selectedProformaInvoice;
        private DateTime? startDate;
        private int? mainCustomerId;
        private int? invoiceCustomerId;
        private int? deliveryCustomerId;

        public PreparationViewModel()
        {
            Initialize();
            StartDate = null;
            EndDate = null;
            LoadProformaInvoices();
        }

        public void LoadProformaInvoices()
        {
            try
            {
                ProformaInvoices = new ObservableCollection<ProformaInvoice>(InvoicingToolComponent.GetLatestInvoiceRuns());
            }
            catch (Exception)
            {
                RadWindow.Alert("Could not load Proformas, there might be a problem with the connection to the database.");
                ProformaInvoices = new ObservableCollection<ProformaInvoice>();
            }
        }

        public ProformaInvoice SelectedProformaInvoice
        {
            get { return selectedProformaInvoice; }
            set
            {
                selectedProformaInvoice = value;
                OnPropertyChanged();
            }
        }

        public ObservableCollection<ProformaInvoice> ProformaInvoices
        {
            get { return proformaInvoices ?? (proformaInvoices = new ObservableCollection<ProformaInvoice>()); }
            set
            {
                proformaInvoices = value;
                OnPropertyChanged();
            }
        }

        public RelayCommand ProformaDeleteCommand
        {
            get
            {
                return new RelayCommand(() =>
                {
                    DeleteProforma(SelectedProformaInvoice.Id);
                }, () => SelectedProformaInvoice != null);
            }
        }

        public RelayCommand ProformaInvoicingCommand
        {
            get
            {
                return new RelayCommand(() =>
                {
                    ViewModelLocator.Processing.LoadData(SelectedProformaInvoice);
                    ViewModelLocator.MoveToProcessing();
                }, () => SelectedProformaInvoice != null);
            }
        }


        public CustomerShipViewModel SelectedCustomership
        {
            get { return selectedCustomership; }
            set
            {
                selectedCustomership = value;
                OnPropertyChanged();
            }
        }

        public CustomerClass SelectedCustomerClass
        {
            get { return selectedCustomerClass; }
            set
            {
                selectedCustomerClass = value;
                OnPropertyChanged();
            }
        }

        public int? MainCustomerId
        {
            get { return mainCustomerId; }
            set
            {
                mainCustomerId = value;
                OnPropertyChanged();
            }
        }

        public int? InvoiceCustomerId
        {
            get { return invoiceCustomerId; }
            set
            {
                invoiceCustomerId = value;
                OnPropertyChanged();
            }
        }

        public int? DeliveryCustomerId
        {
            get { return deliveryCustomerId; }
            set
            {
                deliveryCustomerId = value;
                OnPropertyChanged();
            }
        }

        public CustomerGroup SelectedCustomerGroup
        {
            get { return selectedCustomerGroup; }
            set
            {
                selectedCustomerGroup = value;
                OnPropertyChanged();
            }
        }


        public InvoicingGroup SelectedInvoicingGroupForeign
        {
            get { return selectedInvoicingGroupForeign; }
            set
            {
                selectedInvoicingGroupForeign = value;
                OnPropertyChanged();
            }
        }


        public InvoicingGroup SelectedInvoicingGroupDomestic
        {
            get { return selectedInvoicingGroupDomestic; }
            set
            {
                selectedInvoicingGroupDomestic = value;
                OnPropertyChanged();
            }
        }

        public DateTime? StartDate
        {
            get { return startDate; }
            set
            {
                startDate = value;
                OnPropertyChanged();
            }
        }

        public DateTime? EndDate
        {
            get { return endDate; }
            set
            {
                endDate = value;
                OnPropertyChanged();
            }
        }

        public ObservableCollection<InvoicingGroup> InvoicingGroups
        {
            get { return invoicingGroups ?? (invoicingGroups = new ObservableCollection<InvoicingGroup>()); }
            set
            {
                invoicingGroups = value;
                OnPropertyChanged();
            }
        }

        public ObservableCollection<CustomerClass> CustomerClasses
        {
            get { return customerClasses ?? (customerClasses = new ObservableCollection<CustomerClass>()); }
            set
            {
                customerClasses = value;
                OnPropertyChanged();
            }
        }

        public ObservableCollection<CustomerGroup> CustomerGroups
        {
            get { return customerGroups ?? (customerGroups = new ObservableCollection<CustomerGroup>()); }
            set
            {
                customerGroups = value;
                OnPropertyChanged();
            }
        }

        public ObservableCollection<CustomerShipViewModel> CustomerShips
        {
            get { return customerShips; }
            set
            {
                customerShips = value;
                OnPropertyChanged();
            }
        }

        public RelayCommand MoveToProcessingCommand
        {
            get
            {
                return new RelayCommand(() =>
                {
                    if (!StartDate.HasValue || !EndDate.HasValue)
                    {
                        DialogParameters parameters = new DialogParameters();
                        parameters.Owner = Application.Current.MainWindow;
                        parameters.Content = "Start Date and End Date must have values.";
                        RadWindow.Alert(parameters);
                        return;
                    }

                    // add timespan to get the last date into query results.
                    if (EndDate.Value.Hour == 0 && EndDate.Value.Minute == 0 && EndDate.Value.Second == 0)
                        EndDate = EndDate.Value.Add(new TimeSpan(23, 59, 59));
                    ViewModelLocator.Processing.LoadData(new SearchCriterion(MainCustomerId, InvoiceCustomerId, DeliveryCustomerId, SelectedCustomership.CustomerShip, 
                        StartDate, EndDate, SelectedCustomerClass, SelectedCustomerGroup, SelectedInvoicingGroupDomestic,
                        SelectedInvoicingGroupForeign));
                    ViewModelLocator.MoveToProcessing();
                }, () => SelectedCustomership != null);
            }
        }

        private void DeleteProforma(int id)
        {
            var confirmed = false;
            DialogParameters parameters = new DialogParameters();
            parameters.Owner = Application.Current.MainWindow;
            parameters.Content = "Are you sure you want to permanently delete this proforma invoice?";
            parameters.Closed = (sender, args) => confirmed = args.DialogResult.HasValue && args.DialogResult.Value;
            RadWindow.Confirm(parameters);

            if (!confirmed) return;

            if (InvoicingToolComponent.DeleteProforma(id))
            {
                proformaInvoices.Remove(selectedProformaInvoice);
            }
        }
        private void Initialize()
        {
            try
            {
                CustomerGroups = new ObservableCollection<CustomerGroup>(InvoicingToolComponent.GetCustomerGroups());
                CustomerClasses =
                    new ObservableCollection<CustomerClass>(InvoicingToolComponent.GetCustomerClasses());
                InvoicingGroups =
                    new ObservableCollection<InvoicingGroup>(InvoicingToolComponent.GetInvoicingGroups());
                SelectedCustomerGroup = CustomerGroups.First();
                SelectedCustomerClass = CustomerClasses.First();
                SelectedInvoicingGroupDomestic = InvoicingGroups.First();
                SelectedInvoicingGroupForeign = InvoicingGroups.First();
                
                CustomerShips = new ObservableCollection<CustomerShipViewModel>
                {
                    new CustomerShipViewModel
                    {
                        CustomerShip = CustomerShip.Finland,
                        Image = "/InvoicingTool.UI;component/Assets/fi.png"
                    },
                    new CustomerShipViewModel
                    {
                        CustomerShip = CustomerShip.Sweden,
                        Image = "/InvoicingTool.UI;component/Assets/se.png"
                    },
                    new CustomerShipViewModel
                    {
                        CustomerShip = CustomerShip.England,
                        Image = "/InvoicingTool.UI;component/Assets/uk.png"
                    },
                    new CustomerShipViewModel
                    {
                        CustomerShip = CustomerShip.Norway,
                        Image = "/InvoicingTool.UI;component/Assets/no.png"
                    },
                    new CustomerShipViewModel
                    {
                        CustomerShip = CustomerShip.Denmark,
                        Image = "/InvoicingTool.UI;component/Assets/dk.png"
                    },
                    new CustomerShipViewModel
                    {
                        CustomerShip = CustomerShip.Netherland,
                        Image = "/InvoicingTool.UI;component/Assets/nl.png"
                    },
                    new CustomerShipViewModel
                    {
                        CustomerShip = CustomerShip.Spain,
                        Image = "/InvoicingTool.UI;component/Assets/es.png"
                    },
                    new CustomerShipViewModel
                    {
                        CustomerShip = CustomerShip.Belgium,
                        Image = "/InvoicingTool.UI;component/Assets/be.png"
                    },
                    new CustomerShipViewModel
                    {
                        CustomerShip = CustomerShip.SouthAfrica,
                        Image = "/InvoicingTool.UI;component/Assets/sa.png"
                    }
                };
            }
            catch (Exception ex)
            {
                ViewModelLocator.RaiseException(ex.Message);
            }
        }
    }

    public class CustomerShipViewModel
    {
        public CustomerShip CustomerShip { get; set; }
        public string Image { get; set; }
    }
}