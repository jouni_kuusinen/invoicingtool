﻿using InvoicingTool.Business;
using InvoicingTool.Entities;
using InvoicingTool.UI.Helpers;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace InvoicingTool.UI.ViewModels
{
    public sealed class OrderRowViewModel : ViewModelBase, INotifyDataErrorInfo
    {
        private const string ORDER_SENT_ERROR = "Couldn't invoice this row.";
        private const string ORDER_ONHOLD_ERROR = "Order row is marked as on hold!";
        private const string ORDER_INVOICELATER_ERROR = "Order row is marked as invoice later!";
        private const string DECIMAL_ERROR = "Value cannot be less or equal to 0.";
        private const string DECIMAL_WARNING = "Value should not be less or equal to 0.";
        private const string TITLE_SUSPENDED = "Title is marked as Suspended!";
        private const string TITLE_DELAYED = "Title is marked as Delayed!";
        private const string OPEN_ACCESS = "Open access!";
        private const string NOT_VIA_AGENTS = "Not via agents!";

        private readonly Dictionary<String, List<string>> Errors =
            new Dictionary<string, List<string>>();

        private OrderRow order;
        private bool selected;
        private bool? sent;
        private readonly BackgroundWorker _worker;

        public OrderRowViewModel(OrderRow order)
        {
            _worker = new BackgroundWorker();
            _worker.DoWork += Worker_DoWork;
                      
            Order = order;
            ValidateObject();
        }

        #region Public Members

        [Browsable(false)]
        public OrderRow Order
        {
            get { return order; }
            set
            {
                order = value;
                OnPropertyChanged();
                OnPropertyChanged(@"GroupId");
                OnPropertyChanged(@"Comment1");
                OnPropertyChanged(@"Comment2");
                OnPropertyChanged(@"MainCustomerId");
                OnPropertyChanged(@"MainCustomer");
                OnPropertyChanged(@"CustomerGroup");
                OnPropertyChanged(@"InvoicingGroupForeign");
                OnPropertyChanged(@"InvoicingGroupDomestic");
                OnPropertyChanged(@"CustomerClass");
                OnPropertyChanged(@"CustomerShip");
                OnPropertyChanged(@"PurchasePaid");
                OnPropertyChanged(@"IsForeign");
                OnPropertyChanged(@"PaymentTerm");
                OnPropertyChanged(@"InvoiceCustomerId");
                OnPropertyChanged(@"InvoiceCustomer");
                OnPropertyChanged(@"DeliveryCustomerId");
                OnPropertyChanged(@"DeliveryCustomer");
                OnPropertyChanged(@"DiscountPercent");
                OnPropertyChanged(@"NetPurchasePrice");
                OnPropertyChanged(@"NetPrice");
                OnPropertyChanged(@"GrossPurchasePrice");
                OnPropertyChanged(@"TitleFrequency");
                OnPropertyChanged(@"GrossMargin");
                OnPropertyChanged(@"TitleCode");
                OnPropertyChanged(@"TitleName");
                OnPropertyChanged(@"TitleFormat");
                OnPropertyChanged(@"TitleCategory");
                OnPropertyChanged(@"TitleCategory2");
                OnPropertyChanged(@"Status");
                OnPropertyChanged(@"Quantity");
                OnPropertyChanged(@"PriceYear");
                OnPropertyChanged(@"InvoicingDelayedUntil");
                OnPropertyChanged(@"PriceValidFrom");
                OnPropertyChanged(@"PriceSource");
                OnPropertyChanged(@"PeriodEnd");
                OnPropertyChanged(@"PeriodStart");
                OnPropertyChanged(@"Months");
                OnPropertyChanged(@"GrossPrice");
                OnPropertyChanged(@"ModifiedPrice");
                OnPropertyChanged(@"ExceptionalPeriod");
                OnPropertyChanged(@"NegativeMargin");
                OnPropertyChanged(@"CustomizedPrice");
                OnPropertyChanged(@"ManualInvoicing");
                OnPropertyChanged(@"Currency");
                OnPropertyChanged(@"Postage");
                OnPropertyChanged(@"Airmail");
                OnPropertyChanged(@"Selected");
                OnPropertyChanged(@"OnHold");
                OnPropertyChanged(@"InvoiceLater");
                OnPropertyChanged(@"Sent");
                OnPropertyChanged(@"ProductGroupID");
                OnPropertyChanged(@"ProductGroup");
                OnPropertyChanged(@"VatDivision");
                OnPropertyChanged(@"SeriesTitle");
                OnPropertyChanged(@"CSR");
                OnPropertyChanged(@"MainTitleStatus");
                OnPropertyChanged(@"OrderStatus");
                OnPropertyChanged(@"ConsolidationFee");
                OnPropertyChanged(@"OriginalCurrency");
                OnPropertyChanged(@"CustomerCurrencyRate");
                OnPropertyChanged(@"Note");
                OnPropertyChanged(@"NoteText");
                OnPropertyChanged(@"LMS");
                OnPropertyChanged(@"FixedPrices");
                OnPropertyChanged(@"PurchaseInvoiceNumber");
                OnPropertyChanged(@"StaticOrderNumber");
                OnPropertyChanged(@"PaymentMethod");
                OnPropertyChanged(@"Medical");
            }
        }

        [Display(Order = 1)]
        public decimal Airmail
        {
            get { return Order.Airmail; }
            set
            {
                Order.Airmail = value;
                OnPropertyChanged();
                OnPropertyChanged(@"NetPrice");
                OnPropertyChanged(@"GrossMargin");
            }
        }

        [Display(Order = 2)]
        [DisplayName("Comment #1")]
        public string Comment1
        {
            get { return Order.Comment1; }
        }

        [Display(Order = 3)]
        [DisplayName("Comment #2")]
        public string Comment2
        {
            get { return Order.Comment2; }
        }

        [Display(Order = 4)]
        public string Consolidation
        {
            get { return Order.Consolidation; }
        }

        [Display(Order = 5)]
        [DisplayName("Consolidation Fee")]
        public decimal ConsolidationFee
        {
            get { return Order.ConsolidationFee;  }
            set
            {
                Order.ConsolidationFee = value;
                OnPropertyChanged();
            }
        }

        [Display(Order = 6)] // assign right one
        public string CostCenter
        {
            get { return Order.CostCenter; }
        }

        [Display(Order = 7)]
        [DisplayName("Currency")]
        public string Currency
        {
            get { return Order.Currency; }
        }

        [Display(Order = 8)]
        [DisplayName("Cust. Order No.")]
        public string CustomerOrderNumber
        {
            get { return Order.CustomerOrderNumber; }
        }

        [Display(Order = 9)]
        [DisplayName("Customer Class")]
        public string CustomerClass
        {
            get { return Order.CustomerClass; }
        }

        [Display(Order = 10)]
        [DisplayName("Customer Currency Rate")]
        public decimal? CustomerCurrencyRate
        {
            get { return Order.CustomerCurrencyRate; }
        }

        [Display(Order = 11)]
        [DisplayName("Customer Group")]
        public string CustomerGroup
        {
            get { return Order.CustomerGroup; }
        }

        [Display(Order = 12)]
        [DisplayName("Customer Service Representative")]
        public string CSR
        {
            get { return Order.CSR; }
        }

        [Display(Order = 13)]
        [DisplayName("Customership")]
        public CustomerShip CustomerShip
        {
            get { return Order.CustomerShip; }
        }

        [Display(Order = 14)]
        [DisplayName("Customized Price")]
        public bool CustomizedPrice
        {
            get { return Order.Title.CustomizedPrice; }
        }

        [Display(Order = 15)]
        [DisplayName("Delivery Customer")]
        public string DeliveryCustomer
        {
            get { return Order.DeliveryCustomer; }
        }

        [Display(Order = 16)]
        [DisplayName("Delivery Customer Id")]
        public string DeliveryCustomerId
        {
            get { return Order.DeliveryCustomerId; }
        }

        [Display(Order = 17)]
        [DisplayName("Discount-%")]
        public decimal DiscountPercent
        {
            get { return Order.DiscountPercent; }
            set
            {
                Order.DiscountPercent = value;
                OnPropertyChanged();
                OnPropertyChanged("NetPrice");
                OnPropertyChanged("GrossMargin");
            }
        }

        [Display(Order = 18)]
        [DisplayName("Domestic/Foreign")]
        public string IsForeign
        {
            get { return Order.IsForeign ? "Foreign" : "Domestic"; }
        }

        [Display(Order = 19)]
        [DisplayName("Electronic Content")]
        public bool EContent
        {
            get { return (Order.ProductGroupID == 101 || Order.ProductGroupID == 102 || Order.ProductGroupID == 103); }
        }

        [Display(Order = 20)]
        [DisplayName("Errors")]
        public bool HasErrors
        {
            get { return Errors.Count > 0; }
        }

        [Display(Order = 21)]
        [DisplayName("Exceptional Period")]
        public bool ExceptionalPeriod
        {
            get { return Order.Months != 12; }
        }

        [Display(Order = 22)]
        [DisplayName("Fixed Prices")]
        public bool FixedPrices
        {
            get { return Order.FixedPrices; }
        }

        [Display(Order = 23)]
        [DisplayName("Format")]
        public string TitleFormat
        {
            get { return Order.Title.Format; }
        }

        [Display(Order = 24)]
        [DisplayName("Frequency")]
        public int TitleFrequency
        {
            get { return Order.Title.Frequency; }
            set
            {
                Order.Title.Frequency = value;
                OnPropertyChanged();
            }
        }

        [Display(Order = 25)]
        [DisplayName("Gross Margin")]
        public decimal GrossMargin
        {
            get
            {
                decimal margin =
                    ((Math.Round(
                        (Math.Round((GrossPrice - Postage - Airmail) * ((100 - DiscountPercent) / 100) + Postage + Airmail,
                            2)), 2)) - NetPurchasePrice);
                NegativeMargin = margin < 0;
                return margin;
            }
        }

        [Display(Order = 26)]
        [DisplayName("Gross Price")]
        public decimal GrossPrice
        {
            get { return Order.GrossPrice; }
            set
            {
                if (!IsGrossPriceValid(value) || GrossPrice == value) return;
                Order.GrossPrice = value;
                ModifiedPrice = true;
                OnPropertyChanged();
                OnPropertyChanged("NetPrice");
                OnPropertyChanged("GrossMargin");
            }
        }

        [Display(Order = 27)]
        [DisplayName("Gross Purchase Price")]
        public decimal GrossPurchasePrice
        {
            get { return Order.GrossPurchasePrice; }
        }

        [Display(Order = 28)]
        [DisplayName("Group")]
        public int GroupId
        {
            get { return Order.GroupId; }
            set
            {
                Order.GroupId = value;
                OnPropertyChanged();
            }
        }

        [Display(Order = 29)]
        [DisplayName("Invoice Customer")]
        public string InvoiceCustomer
        {
            get { return Order.InvoiceCustomer; }
        }

        [Display(Order = 30)]
        [DisplayName("Invoice Customer Id")]
        public string InvoiceCustomerID
        {
            get { return Order.InvoiceCustomerId; }
        }

        [Display(Order = 31)]
        [DisplayName("Invoice Later")]
        public bool InvoiceLater
        {
            get { return Order.InvoiceLater; }
        }

        [Display(Order = 32)]
        [DisplayName("Invoicing Delayed Until")]
        public DateTime? InvoicingDelayedUntil
        {
            get { return Order.InvoicingDelayedUntil; }
        }

        [Display(Order = 33)]
        [DisplayName("Invoicing Group Domestic")]
        public string InvoicingGroupDomestic
        {
            get { return Order.InvoicingGroupDomestic; }
        }

        [Display(Order = 34)]
        [DisplayName("Invoicing Group Foreign")]
        public string InvoicingGroupForeign
        {
            get { return Order.InvoicingGroupForeign; }
        }

        [Display(Order = 35)]
        [DisplayName("Last Invoice Date")]
        public DateTime? LastInvoiceDate
        {
            get { return Order.LastInvoiceDate; }
        }

        [Display(Order = 36)]
        [DisplayName("LMS Number")]
        public string LMS
        {
            get { return Order.LMS; }
        }

        [Display(Order = 37)]
        [DisplayName("LMSU10 Static Order Number")]
        public string StaticOrderNumber
        {
            get { return Order.StaticOrderNumber; }
        }

        [Display(Order = 38)]
        [DisplayName("Main Customer")]
        public string MainCustomer
        {
            get { return Order.MainCustomer; }
        }

        [Display(Order = 39)]
        [DisplayName("Main Customer Id")]
        public string MainCustomerId
        {
            get { return Order.MainCustomerId; }
        }

        [Display(Order = 40)]
        [DisplayName("Main Title Status")]
        public string MainTitleStatus
        {
            get { return Order.MainTitleStatus; }
        }

        [Display(Order = 41)]
        [DisplayName("Manual Invoicing")]
        public bool ManualInvoicing
        {
            get { return Order.ManualInvoicing; }
        }

        [Display(Order = 42)]
        [DisplayName("Marked")]
        public bool Selected
        {
            get { return selected; }
            set
            {
                selected = value;
                OnPropertyChanged();
            }
        }

        [Display(Order = 43)]
        public bool Medical
        {
            get { return Order.Medical; }
        }

        [Display(Order = 44)]
        [DisplayName("Modified Price")]
        public bool ModifiedPrice
        {
            get { return Order.ModifiedPrice; }
            set
            {
                Order.ModifiedPrice = value;
                OnPropertyChanged();
            }
        }

        [Display(Order = 45)]
        [DisplayName("Months")]
        public int Months
        {
            get { return Order.Months; }
        }

        [Display(Order = 46)]
        [DisplayName("Negative Margin")]
        public bool NegativeMargin
        {
            get { return Order.NegativeMargin; }

            set
            {
                Order.NegativeMargin = value;
                OnPropertyChanged();
            }
        }

        [Display(Order = 47)]
        [DisplayName("Net Price")]
        public decimal NetPrice
        {
            get
            {
                return
                    Math.Round(
                        (Math.Round((GrossPrice - Postage - Airmail) * ((100 - DiscountPercent) / 100) + Postage + Airmail,
                            2)), 2);
            }
        }

        [Display(Order = 48)]
        [DisplayName("Net Purchase Price")]
        public decimal NetPurchasePrice
        {
            get { return Order.NetPurchasePrice; }

        }

        [Display(Order = 49)]
        [DisplayName("Note")]
        public bool Note
        {
            get { return Order.Note; }
            set {
                Order.Note = value;
                OnPropertyChanged(); 
            }
        }

        [Display(Order = 50)]
        [DisplayName("Note Text")]
        public string NoteText
        {
            get {
                Order.Note = (Order.NoteText.Length > 0);

                return Order.NoteText;
            }
            set
            {
                Order.NoteText = value;
                OnPropertyChanged();
                OnPropertyChanged("Note");
            }
        }

        [Display(Order = 51)]
        [DisplayName("On Hold")]
        public bool OnHold
        {
            get { return Order.OnHold; }
        }

        [Display(Order = 52)]
        [DisplayName("Order Row")]
        public string OrderRowNumber
        {
            get { return String.Format("{0}-{1}", Order.OrderId, Order.OrderNumber); }
        }

        [Display(Order = 53)]
        [DisplayName("Type")]
        public string OrderStatus
        {
            get { return Order.OrderStatus; }
        }

        [Display(Order = 54)]
        [DisplayName("Original Currency")]
        public string OriginalCurrency
        {
            get { return Order.OriginalCurrency; }
        }

        [Display(Order = 55)]
        [DisplayName("Payment Method")]
        public string PaymentMethod
        {
            get { return Order.PaymentMethod; }
        }

        [Display(Order = 56)]
        [DisplayName("Payment Term")]
        public string PaymentTerm
        {
            get { return Order.PaymentTerm; }
        }

        [Display(Order = 57)]
        [DisplayName("Period End")]
        public DateTime PeriodEnd
        {
            get { return Order.PeriodEnd; }
        }

        [Display(Order = 58)]
        [DisplayName("Period Start")]
        public DateTime PeriodStart
        {
            get { return Order.PeriodStart; }
        }

        [Display(Order = 59)]
        [DisplayName("Product Group 2")]
        public string TitleCategory2
        {
            get { return Order.Title.Category2; }
        }

        [Display(Order = 60)]
        [DisplayName("Publisher Name")]
        public string PublisherName
        {
            get { return Order.PublisherName; }
        }

        [Display(Order = 61)]
        public decimal Postage
        {
            get { return Order.Postage; }
            set
            {
                Order.Postage = value;
                OnPropertyChanged();
                OnPropertyChanged("NetPrice");
                OnPropertyChanged("GrossMargin");
            }
        }

        [Display(Order = 62)]
        [DisplayName("Price Source")]
        public string PriceSource
        {
            get { return Order.PriceSource; }
        }

        [Display(Order = 63)]
        [DisplayName("Price Valid From")]
        public DateTime PriceValidFrom
        {
            get { return Order.PriceValidFrom; }
        }

        [Display(Order = 64)]
        [DisplayName("Price Year")]
        public int PriceYear
        {
            get { return Order.PriceYear; }
        }

        [Display(Order = 65)]
        [DisplayName("Product Group")]
        public string ProductGroup
        {
            get { return Order.ProductGroup; }
        }

        [Display(Order = 66)]
        [DisplayName("Purchase Invoice Number")]
        public string PurchaseInvoiceNumber
        {
            get { return Order.PurchaseInvoiceNumber; }
        }

        [Display(Order = 67)]
        [DisplayName("Purchase Invoice Paid")]
        public DateTime? PurchasePaid
        {
            get { return Order.PurchasePaid; }
        }

        [Display(Order = 68)]
        [DisplayName("Quantity")]
        public int Quantity
        {
            get { return Order.Quantity; }
            set
            {
                Order.Quantity = value;
                OnPropertyChanged();
            }
        }

        [Display(Order = 69)]
        public bool? Sent
        {
            get { return sent; }
            set
            {
                SentSucceeded(value);
                sent = value;
                OnPropertyChanged();
            }
        }

        [Display(Order = 70)]
        [DisplayName("Series-title")]
        public bool SeriesTitle
        {
            get { return Order.SeriesTitle; }
        }

        [Display(Order = 71)]
        [DisplayName("Status")]
        public string Status
        {
            get { return Order.Status; }
        }

        [Display(Order = 72)]
        [DisplayName("Title Code")]
        public string TitleCode
        {
            get { return Order.Title.TitleCode; }
        }

        [Display(Order = 73)]
        [DisplayName("Title Name")]
        public string TitleName
        {
            get { return Order.Title.TitleName; }
        }

        [Display(Order = 74)]
        [DisplayName("VAT Division")]
        public decimal? VatDivision
        {
            get { return Order.VatDivision; }
        }

        [Display(Order = 75)]
        [DisplayName("VAT Percent")]
        public decimal VatPercent
        {
            get { return Order.VatPercent; }
        }

        #endregion

        #region Methods

        #region Calculation

        public void AddToStubPeriods(decimal additionPercentage, decimal additionValue)
        {
            if (!Order.PriceFixed)
            {
                GrossPrice =
                    Math.Round(
                        GrossPrice * Math.Ceiling(Math.Round((decimal)((PeriodEnd.AddDays(1) - PeriodStart).Days / 30), 1)) / 12 *
                        ((100 + additionPercentage) / 100) + additionValue, 2, MidpointRounding.AwayFromZero);
            }
        }

        public void AddToPublishersPrice(decimal additionPercentage, decimal additionValue)
        {
            if (!Order.PriceFixed)
            {
                GrossPrice =
                    Math.Round(
                        ((GrossPurchasePrice * ((100 + additionPercentage) / 100) + additionValue)) *
                        (GrossPrice / GrossPurchasePrice), 2, MidpointRounding.AwayFromZero);
            }
        }

        public void AddToNetPurchasePrice(decimal additionPercentage, decimal additionValue)
        {
            if (!Order.PriceFixed)
            {
                GrossPrice = Math.Round(NetPurchasePrice * ((100 + additionPercentage) / 100) + additionValue, 2, MidpointRounding.AwayFromZero);

            }
        }

        public void AddToCustomersSalesPrice(decimal additionPercentage, decimal additionValue)
        {
            if (!Order.PriceFixed)
            {
                GrossPrice = Math.Round(GrossPrice * ((100 + additionPercentage) / 100) + additionValue, 2);
            }
        }

        #endregion

        #region Validation

        private void ValidateObject()
        {
            IsGrossPriceValid(Order.GrossPrice);
            IsOrderRowValid(Order);
        }

        public void SentSucceeded(bool? value)
        {
            if (value.HasValue && !value.Value)
            {
                AddError("OrderRowNumber", ORDER_SENT_ERROR, false);
            }
            else
            {
                RemoveError("OrderRowNumber", ORDER_SENT_ERROR);
            }
        }

        //public void IsNegativeMargin(bool value)
        //{
        //    if (value)
        //    {
        //        AddError("OrderRowNumber", ORDER_MARGIN_WARNING, true);
        //    }
        //    else RemoveError("OrderRowNumber", ORDER_MARGIN_WARNING);
        //}

        private bool IsGrossPriceValid(decimal value, bool isZeroAllowed = false)
        {
            if (value > 0)
            {
                RemoveError("GrossPrice", DECIMAL_ERROR);
                RemoveError("GrossPrice", DECIMAL_WARNING);
                return true;
            }

            if (isZeroAllowed)
            {
                AddError("GrossPrice", DECIMAL_WARNING, true);
            }
            else
            {
                AddError("GrossPrice", DECIMAL_ERROR, false);
            }

            return false;
        }

        private void IsOrderRowValid(OrderRow orderRow)
        {
            if (orderRow.NotViaAgents)
                AddError("OrderRowNumber", NOT_VIA_AGENTS, false);
            else
                RemoveError("OrderRowNumber", NOT_VIA_AGENTS);

            if (orderRow.OnHold)
                AddError("OrderRowNumber", ORDER_ONHOLD_ERROR, false);
            else
                RemoveError("OrderRowNumber", ORDER_ONHOLD_ERROR);

            if (orderRow.InvoiceLater)
                AddError("OrderRowNumber", ORDER_INVOICELATER_ERROR, false);
            else
                RemoveError("OrderRowNumber", ORDER_INVOICELATER_ERROR);

            if (orderRow.MainTitleStatus == "Delayed")
                AddError("OrderRowNumber", TITLE_DELAYED, false);
            else
                RemoveError("OrderRowNumber", TITLE_DELAYED);

            if (orderRow.MainTitleStatus == "Suspended")
                AddError("OrderRowNumber", TITLE_SUSPENDED, false);
            else
                RemoveError("OrderRowNumber", TITLE_SUSPENDED);

            if (orderRow.Title.Format.Contains("open access"))
                AddError("OrderRowNumber", OPEN_ACCESS, false);
            else
                RemoveError("OrderRowNumber", OPEN_ACCESS);
        }

        /// <summary>
        /// Adds the specified error to the errors collection if it is not 
        /// already present, inserting it in the first position if isWarning is 
        /// false. Raises the ErrorsChanged event if the collection changes.
        /// </summary>
        /// <param name="propertyName"></param>
        /// <param name="error"></param>
        /// <param name="isWarning"></param>
        private void AddError(string propertyName, string error, bool isWarning)
        {
            if (!Errors.ContainsKey(propertyName))
                Errors[propertyName] = new List<string>();

            if (!Errors[propertyName].Contains(error))
            {
                Errors[propertyName].Add(error);
                //if (isWarning) errors[propertyName].Add(err);
                //else errors[propertyName].Insert(0, err);
                RaiseErrorsChanged(propertyName);
            }
        }

        /// <summary>
        /// Removes the specified error from the errors collection if it is
        /// present. Raises the ErrorsChanged event if the collection changes.
        /// </summary>
        /// <param name="propertyName"></param>
        /// <param name="error"></param>
        private void RemoveError(string propertyName, string error)
        {
            if (Errors.ContainsKey(propertyName) &&
                Errors[propertyName].Contains(error))
            {
                Errors[propertyName].Remove(error);
                if (Errors[propertyName].Count == 0) Errors.Remove(propertyName);
                RaiseErrorsChanged(propertyName);
            }
        }

        private void RaiseErrorsChanged(string propertyName)
        {
            if (ErrorsChanged != null)
                ErrorsChanged(this, new DataErrorsChangedEventArgs(propertyName));
        }

        #endregion

        public void ReloadAsync()
        {
            if (_worker.IsBusy == false)
            {
                _worker.RunWorkerAsync();
            }
        }

        public void Reload(bool quick = false, bool onlypurchases = false)
        {
            var group = Order.GroupId;
            if (onlypurchases)
            {
                Order = InvoicingToolComponent.GetOrderRow(Order, true);
            }
            else
            {
                if (quick)
                {
                    Order = InvoicingToolComponent.GetOrderRow(Order);
                }
                else
                {
                    Order = InvoicingToolComponent.GetOrderRow(Order.OrderRowId, Order.CustomerShip);
                }
            }
            
            ValidateObject();
            Order.GroupId = @group;
        }

        public void CancelReload()
        {
            _worker.CancelAsync();
        }

        #endregion

        #region INotifyDataErrorInfo Members

        public event EventHandler<DataErrorsChangedEventArgs> ErrorsChanged;

        public IEnumerable GetErrors(string propertyName)
        {
            if (String.IsNullOrEmpty(propertyName) ||
                !Errors.ContainsKey(propertyName)) return null;
            return Errors[propertyName];
        }

        #endregion

        private void Worker_DoWork(object sender, DoWorkEventArgs e)
        {          
            var group = Order.GroupId;
            Order = InvoicingToolComponent.GetOrderRow(Order.OrderRowId, Order.CustomerShip);               
            ValidateObject();
            Order.GroupId = @group;
        }

    }
}