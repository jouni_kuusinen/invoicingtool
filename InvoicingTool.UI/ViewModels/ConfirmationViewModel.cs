﻿using GalaSoft.MvvmLight.CommandWpf;
using InvoicingTool.Business;
using InvoicingTool.Entities;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using Telerik.Windows.Controls;
using ViewModelBase = InvoicingTool.UI.Helpers.ViewModelBase;

namespace InvoicingTool.UI.ViewModels
{
    public class ConfirmationViewModel : ViewModelBase
    {
        private ObservableCollection<OrderRowViewModel> _orderRows;
        private ObservableCollection<OrderRowViewModel> _toBeRemoved;
        private ObservableCollection<Notification> _notifications;
        private readonly BackgroundWorker _worker;
        private bool _invoice;
        private bool _removeRows;

        public ConfirmationViewModel()
        {
            IsBackgroundVisible = false;
            _worker = new BackgroundWorker()
            {
                WorkerReportsProgress = true
            };
            _worker.DoWork += Worker_DoWork;
            _worker.RunWorkerCompleted += Worker_RunWorkerCompleted;
            _worker.ProgressChanged += Worker_ProgressChanged;

            _toBeRemoved = new ObservableCollection<OrderRowViewModel>();
        }

        public ObservableCollection<OrderRowViewModel> OrderRows
        {
            get { return _orderRows; }
            set
            {
                _orderRows = value;
                OnPropertyChanged();
            }
        }

        private int _currentProgress;
        public int CurrentProgress 
        {
            get { return _currentProgress; }
            set { _currentProgress = value; OnPropertyChanged(); }
        }

        private bool _isBackgroundVisible;
        public bool IsBackgroundVisible
        {
            get
            {
                return _isBackgroundVisible;
            }
            set
            {
                _isBackgroundVisible = value;
                OnPropertyChanged();
            }
        }

        private bool _isProformaLocked;
        public bool IsProformaLocked
        {
            get
            {
                return _isProformaLocked;
            }
            set
            {
                _isProformaLocked = value;
                OnPropertyChanged();
            }
        }

        private ProformaInvoice _proformaInvoice;
        public ProformaInvoice ProformaInvoice
        {
            get { return _proformaInvoice; }
            set
            {
                _proformaInvoice = value;
                OnPropertyChanged();
            }
        }

        private void Worker_DoWork(object sender, DoWorkEventArgs e)
        {
            var i = 0;
            var maxRecords = OrderRows.Count(x => x.Selected);

            foreach (var orderRow in OrderRows.Where(x => x.Selected))
            {
                i++;
                _worker.ReportProgress(Convert.ToInt32(((decimal) i/(decimal) maxRecords)*100));
                var result = InvoicingToolComponent.ConfirmOrderRow(orderRow.Order, _invoice);
                orderRow.Selected = !_removeRows || !result;
                orderRow.Sent = !_invoice ? null : (bool?) result;

                if (result && _removeRows)
                    _toBeRemoved.Add(orderRow);
            }
        }

        private void Worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            CurrentProgress = 0;
            IsBackgroundVisible = false;
            foreach (var orderRow in _toBeRemoved)
            {
                ViewModelLocator.Processing.OrderRows.Result.Remove(orderRow);
            }
            _toBeRemoved.Clear();
        }

        private void Worker_ProgressChanged(object sender, ProgressChangedEventArgs progressChangedEventArgs)
        {
            CurrentProgress = progressChangedEventArgs.ProgressPercentage;
            if (!IsBackgroundVisible && CurrentProgress > 0)
            {
                IsBackgroundVisible = true;
            }
        }


        public RelayCommand InvoiceCommand
        {
            get
            {
                return new RelayCommand(() => InvoiceExecute(true, true),
                    () => OrderRows != null && OrderRows.Any(x => (x.Selected && x.Sent == null) || (x.Selected && x.Sent.HasValue && !x.Sent.Value)));
            }
        }

        public RelayCommand ProformaCommand
        {
            get
            {
                return new RelayCommand(ProformaExecute,
                () => OrderRows != null && OrderRows.Any(x => (x.Selected && x.Sent == null) || (x.Selected && x.Sent.HasValue && !x.Sent.Value)));
            }
        }

        private void ProformaExecute()
        {
            if (InvoiceExecute(true))
            {
                IsProformaLocked = true;
            }
        }

        public RelayCommand ReleaseProformaCommand
        {
            get { return new RelayCommand(ReleaseProformaExecute); }
        }

        private void ReleaseProformaExecute()
        {
            InvoiceExecute(false);
            IsProformaLocked = false;
        }

        public ObservableCollection<Notification> Notifications
        {
            get { return _notifications ?? (_notifications = new ObservableCollection<Notification>()); }
            set
            {
                _notifications = value;
                OnPropertyChanged();
            }
        }

        private bool InvoiceExecute(bool invoice, bool removeRows = false)
        {
            var confirmed = false;

            if (_worker.IsBusy) return false;

            DialogParameters parameters = new DialogParameters();
            parameters.Owner = System.Windows.Application.Current.MainWindow;

            if (invoice)
            {
                parameters.Content = "Are you sure you want to proceed?";
                parameters.Closed = (sender, args) => confirmed = args.DialogResult.HasValue && args.DialogResult.Value;
                RadWindow.Confirm(parameters);
            }
            else
            {
                string name = null;
                parameters.Content = "Please give a name for this proforma invoice run.";
                parameters.Closed = (s, e) => name = e.PromptResult;
                
                RadWindow.Prompt(parameters);
                if (name != null)
                    ProformaInvoice = InvoicingToolComponent.CreateProformaInvoice(OrderRows.Where(x => x.Selected).Select(x => x.Order.OrderRowId), name);
            }

            if (invoice && !confirmed) return false;
            
            var errors = OrderRows.Where(x => x.Selected && x.HasErrors);

            if (errors.Any())
            {
                AddNotification(NotificationType.Error, "One or more errors in the order rows selected for invoicing, invoicing aborted.");
                return false;
            }
            _invoice = invoice;
            _removeRows = removeRows;
            _worker.RunWorkerAsync();
            return true;
        }

        private void AddNotification(NotificationType type, string text)
        {
            Notifications = new ObservableCollection<Notification>
            {
                new Notification
                {
                    Id = Notifications.Any() ? Notifications.Max(x => x.Id + 1) : 0,
                    Text = text,
                    Type = type
                }
            };
        }
    }
}
