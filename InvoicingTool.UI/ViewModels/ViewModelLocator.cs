﻿
using GalaSoft.MvvmLight.CommandWpf;
using Microsoft.Practices.EnterpriseLibrary.Common.Utility;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using Telerik.Windows.Controls;

namespace InvoicingTool.UI.ViewModels
{
    public sealed class ViewModelLocator
    {
        private static PreparationViewModel preparation;
        private static ConfirmationViewModel confirmation;
        private static ProcessingViewModel processing;

        public static Stream Stream { get; set; }

        public static PreparationViewModel Preparation
        {
            get { return preparation ?? (preparation = new PreparationViewModel()); }
        }

        public static ProcessingViewModel Processing
        {
            get { return processing ?? (processing = new ProcessingViewModel()); }
        }

        public static ConfirmationViewModel Confirmation
        {
            get { return confirmation ?? (confirmation = new ConfirmationViewModel()); }
        }

        public static RelayCommand MoveToPreparationCommand
        {
            get
            {
                return new RelayCommand(MoveToPreparation);
            }
        }

        public static RelayCommand MoveToProcessingCommand
        {
            get
            {
                return new RelayCommand(MoveToProcessing);
            }
        }

        public static RelayCommand MoveToConfirmationCommand
        {
            get
            {
                return new RelayCommand(MoveToConfirmation,
                    () => Processing != null && Processing.OrderRows != null && Processing.OrderRows.Result != null && Processing.OrderRows.Result.Any(x => x.Selected));
            }
        }

        public static void MoveToPreparation()
        {
            var confirmed = false;
            DialogParameters parameters = new DialogParameters
            {
                Owner = Application.Current.MainWindow,
                Content = "Are you sure? You'll lose any unsaved changes.",
                Closed = (sender, args) => confirmed = args.DialogResult.HasValue && args.DialogResult.Value
            };
            RadWindow.Confirm(parameters);

            if (!confirmed)
            {
                return;
            }

            Initialize();
            Preparation.LoadProformaInvoices();
            Navigator.NavigationService.Navigate(Navigator.PreparationView);
        }

        private static void Initialize()
        {
            Navigator.ProcessingView.DataFilter.FilterDescriptors.Clear();
            Stream = null;
            Processing.CancelReload();
            Processing.CustomerData = null;
            Processing.DiscountPercentage = 0M;
            Processing.AdditionPercentage = 0M;
            Processing.AdditionValue = 0M;
            Processing.ProformaInvoice = null;
            if (Processing.Cts != null)
                Processing.Cts.Cancel();
            Confirmation.ProformaInvoice = null;
        }

        public static void MoveToProcessing()
        {
            Navigator.NavigationService.Navigate(Navigator.ProcessingView);
            if (Confirmation.OrderRows != null)
            {
                Confirmation.OrderRows.Clear();
            }
            else
            {
                Confirmation.OrderRows = new ObservableCollection<OrderRowViewModel>();
            }
        }

        public static void MoveToConfirmation()
        {
            Confirmation.Notifications = new ObservableCollection<Notification>();
            Navigator.NavigationService.Navigate(Navigator.ConfirmationView);
            Confirmation.ProformaInvoice = Processing.ProformaInvoice;
            Processing.OrderRows.Result.Where(x => x.Selected).ForEach(Confirmation.OrderRows.Add);
        }

        public RelayCommand<CancelEventArgs> CloseCommand
        {
            get
            {
                return new RelayCommand<CancelEventArgs>(Shutdown);
            }
        }

        public static void RaiseException(string message)
        {
            Application.Current.Dispatcher.BeginInvoke(new Action(() => RadWindow.Alert(new TextBlock
            {
                Width = 350,
                TextWrapping = TextWrapping.Wrap,
                Text = message
            })));
        }

        private void Shutdown(CancelEventArgs cancelEventArgs)
        {
            DialogParameters parameters = new DialogParameters();
            parameters.Owner = Application.Current.MainWindow;
            parameters.Content = "Are you sure you want to close InvoicingTool?";
            parameters.Closed = (sender, args) =>
            {
                if (args.DialogResult.HasValue && args.DialogResult.Value)
                {
                    cancelEventArgs.Cancel = false;
                }
                else
                {
                    cancelEventArgs.Cancel = true;
                }
            };

            RadWindow.Confirm(parameters);
        }
    }
}
