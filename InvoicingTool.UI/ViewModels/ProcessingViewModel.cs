﻿using InvoicingTool.Business;
using InvoicingTool.Entities;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using Telerik.Windows.Controls;
using ViewModelBase = InvoicingTool.UI.Helpers.ViewModelBase;

namespace InvoicingTool.UI.ViewModels
{

    public sealed class ProcessingViewModel : ViewModelBase
    {
        private NotifyTaskCompletion<ObservableCollection<OrderRowViewModel>> orderRows;
        private readonly BackgroundWorker _worker;
        private bool QuickLoad = false;
        private bool OnlyPurchases = false;
        private SearchCriterion searchCriterion;
        public CancellationTokenSource Cts;

        public ProcessingViewModel()
        {
            _worker = new BackgroundWorker()
            {
                WorkerReportsProgress = true
            };
            _worker.DoWork += Worker_DoWork;
            _worker.WorkerSupportsCancellation = true;
            _worker.RunWorkerCompleted += Worker_RunWorkerCompleted;
            _worker.ProgressChanged += Worker_ProgressChanged;
            IsBackgroundVisible = false;
        }

        public void LoadData(SearchCriterion criterion)
        {
            if (Cts == null)
            {
                Cts = new CancellationTokenSource();
            }

            searchCriterion = criterion;
            OrderRows = new NotifyTaskCompletion<ObservableCollection<OrderRowViewModel>>(t => GetOrderRowsAsync(criterion, Cts.Token), Cts.Token);
            OrderRows.Task.ContinueWith(DisplayData, Cts.Token, TaskContinuationOptions.OnlyOnRanToCompletion, TaskScheduler.Current);
            OrderRows.Task.ContinueWith(DisposeCTS, TaskContinuationOptions.PreferFairness);
        }

        private void Worker_DoWork(object sender, DoWorkEventArgs e)
        {
            var i = 0;
            var maxRecords = OrderRows.Result.Count;

            Parallel.ForEach(OrderRows.Result, new ParallelOptions { MaxDegreeOfParallelism = 3 }, (item) =>
            {
                if (_worker.CancellationPending)
                {
                    e.Cancel = true;
                    return;
                }
                Interlocked.Increment(ref i);
                _worker.ReportProgress(Convert.ToInt32(((decimal)i / (decimal)maxRecords) * 100));
                var rivi = (OrderRowViewModel)item;

                rivi.Reload(QuickLoad, OnlyPurchases);
            });
        }

        private void Worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            CurrentProgress = 0;
            IsBackgroundVisible = false;
        }

        private void Worker_ProgressChanged(object sender, ProgressChangedEventArgs progressChangedEventArgs)
        {
            CurrentProgress = progressChangedEventArgs.ProgressPercentage;
            if (CurrentProgress > 0 && !IsBackgroundVisible)
            {
                IsBackgroundVisible = true;
            }
        }

        public void ReloadAll(bool loadOnlyPurchases = false)
        {
            if (_worker.IsBusy == false)
            {
                QuickLoad = false;
                OnlyPurchases = loadOnlyPurchases;
                _worker.RunWorkerAsync();
            }
        }

        public void CancelReload()
        {
            _worker.CancelAsync();
        }

        private void DisplayData(Task previousTask)
        {
            if (OrderRows != null && OrderRows.Result != null)
            {
                var first = OrderRows.Result.FirstOrDefault();
                if (first != null)
                {
                    CustomerData = OrderRows.Result.All(x => x.MainCustomerId == first.MainCustomerId) ? String.Format("{0} : {1}, ", first.MainCustomerId, first.MainCustomer) : "";
                }

                if (_worker.IsBusy == false)
                {
                    QuickLoad = true;
                    _worker.RunWorkerAsync();
                }
            }

            if (searchCriterion != null)
            {
                if (searchCriterion.InvoiceCustomerId != null)
                {
                    CustomerData += "Invoice Customer: " + searchCriterion.InvoiceCustomerId + ", ";
                }


                if (searchCriterion.DeliveryCustomerId != null)
                {
                    CustomerData += "Delivery Customer: " + searchCriterion.DeliveryCustomerId + ", ";
                }

                if (searchCriterion.StartDate.HasValue && searchCriterion.EndDate.HasValue)
                {
                    CustomerData += "Date Between " + searchCriterion.StartDate.Value.ToString("dd.MM.yyyy") + " and " + searchCriterion.EndDate.Value.ToString("dd.MM.yyyy");
                }

                if (searchCriterion.CustomerClass != null)
                {
                    CustomerData += ", Customer Class: " + searchCriterion.CustomerClass.Name;
                }

                if (searchCriterion.CustomerGroup != null)
                {
                    CustomerData += ", Customer Group: " + searchCriterion.CustomerGroup.Name;
                }

                if (searchCriterion.InvoicingGroupDomestic != null)
                {
                    CustomerData += ", Invoicing Group Domestic: " + searchCriterion.InvoicingGroupDomestic.Name;
                }

                if (searchCriterion.InvoicingGroupForeign != null)
                {
                    CustomerData += ", Invoicing Group Foreign: " + searchCriterion.InvoicingGroupForeign.Name;
                }
            }
        }

        private void DisposeCTS(Task previousTask)
        {
            if (Cts != null)
            {
                Cts.Dispose();
                Cts = null;
            }
            
        }

        private string customerData;
        public string CustomerData
        {
            get { return customerData; }
            set
            {
                customerData = value;
                OnPropertyChanged();
            }
        }

        public void LoadData(ProformaInvoice invoice)
        {
            OrderRows = new NotifyTaskCompletion<ObservableCollection<OrderRowViewModel>>(t => GetInvoiceOrderRowsAsync(invoice.Id), new CancellationToken(false));
            ProformaInvoice = invoice;
        }

        private Task<ObservableCollection<OrderRowViewModel>> GetInvoiceOrderRowsAsync(int invoiceRunId)
        {
            return Task<ObservableCollection<OrderRowViewModel>>.Factory.StartNew(() =>
            {
                try
                {
                    return new ObservableCollection<OrderRowViewModel>(
                        InvoicingToolComponent.GetInvoiceRunOrders(invoiceRunId).Select(x => new OrderRowViewModel(x)));
                }
                catch (Exception)
                {
                    return null;
                }
            });
        }

        private int _currentProgress;
        public int CurrentProgress
        {
            get { return _currentProgress; }
            set { _currentProgress = value; OnPropertyChanged(); }
        }

        private bool _isBackgroundVisible;
        public bool IsBackgroundVisible
        {
            get
            {
                return _isBackgroundVisible;
            }
            set
            {
                _isBackgroundVisible = value;
                OnPropertyChanged();
            }
        }

        private ProformaInvoice proformaInvoice;
        public ProformaInvoice ProformaInvoice
        {
            get { return proformaInvoice; }
            set
            {
                proformaInvoice = value;
                OnPropertyChanged();
            }
        }

        private decimal discountPercentage;
        public decimal DiscountPercentage
        {
            get { return discountPercentage; }
            set
            {
                discountPercentage = value;
                OnPropertyChanged();
            }
        }

        private decimal consolFee;
        public decimal ConsolFee
        {
            get { return consolFee; }
            set
            {
                consolFee = value;
                OnPropertyChanged();
            }
        }
        private decimal additionValue;
        public decimal AdditionValue
        {
            get { return additionValue; }
            set
            {
                additionValue = value;
                OnPropertyChanged();
            }
        }

        private decimal additionPercentage;
        public decimal AdditionPercentage
        {
            get { return additionPercentage; }
            set
            {
                additionPercentage = value;
                OnPropertyChanged();
            }
        }

        private Task<ObservableCollection<OrderRowViewModel>> GetOrderRowsAsync(SearchCriterion criterion, CancellationToken token)
        {
            return Task<ObservableCollection<OrderRowViewModel>>.Factory.StartNew(t =>
            {
                try
                {
                    return new ObservableCollection<OrderRowViewModel>(
                        InvoicingToolComponent.GetOrderRows(criterion).Select(x => new OrderRowViewModel(x)));

                }
                catch (Exception ex)
                {
                    ViewModelLocator.RaiseException(ex.Message);
                }

                return null;

            }, token);
        }

        public NotifyTaskCompletion<ObservableCollection<OrderRowViewModel>> OrderRows
        {
            get { return orderRows; }
            set
            {
                orderRows = value;
                OnPropertyChanged();
            }
        }

        private bool isOptionsExpanded;
        public bool IsOptionsExpanded
        {
            get { return isOptionsExpanded; }
            private set
            {
                isOptionsExpanded = value;
                OnPropertyChanged();
            }
        }

        private bool percentageConsolidationFee;
        public bool PercentageConsolidationFee
        {
            get { return percentageConsolidationFee; }
            private set
            {
                percentageConsolidationFee = value;
                OnPropertyChanged();
            }
        }

        private bool multiplyConsolidationFee;
        public bool MultiplyConsolidationFee
        {
            get { return multiplyConsolidationFee; }
            private set
            {
                multiplyConsolidationFee = value;
                OnPropertyChanged();
            }
        }

        public enum AddPriceTo
        {
            StubPeriods = 0,
            PublishersPrice = 1,
            CustomersSalesPrice
        }
    }
}
