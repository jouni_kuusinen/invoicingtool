﻿using System.Windows;
using System.Windows.Controls;

namespace InvoicingTool.UI
{
    public class GridViewRowNumberColumn : Telerik.Windows.Controls.GridViewColumn
    {
        public override FrameworkElement CreateCellElement(Telerik.Windows.Controls.GridView.GridViewCell cell, object dataItem)
        {
            var textBlock = cell.Content as TextBlock ?? new TextBlock();
            textBlock.MinWidth = 30;
            textBlock.Padding = new Thickness(3,3,3,3);
            textBlock.TextAlignment = TextAlignment.Center;
            textBlock.Text = string.Format("{0}", DataControl.Items.IndexOf(dataItem) + 1);
            return textBlock;
        }

        protected override void OnPropertyChanged(System.ComponentModel.PropertyChangedEventArgs args)
        {
            base.OnPropertyChanged(args);

            if (args.PropertyName == "DataControl")
            {
                if (DataControl != null && DataControl.Items != null)
                {
                    DataControl.Items.CollectionChanged += (s, e) =>
                    {
                        if (e.Action == System.Collections.Specialized.NotifyCollectionChangedAction.Remove)
                            Refresh();
                    };
                }
            }
        }
    }
}
