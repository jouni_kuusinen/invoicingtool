﻿using System;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using Telerik.Windows.Automation.Peers;
using Telerik.Windows.Controls;
using Telerik.Windows.Input.Touch;
using Squirrel;

namespace InvoicingTool.UI
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {

        public App()
        {
            AutomationManager.AutomationMode = AutomationMode.Disabled;
            TouchManager.IsTouchEnabled = false;
            StyleManager.ApplicationTheme = new Windows8Theme();
            InitializeComponent();

            DispatcherUnhandledException += (s, e) => RadWindow.Alert(new DialogParameters {
                    Content = new TextBlock {
                        Text = ExceptionToText(e.Exception),
                        TextWrapping = TextWrapping.Wrap
                    },
                    Header = "Unhandled error!",
                    DialogStartupLocation = WindowStartupLocation.CenterScreen,
                    Owner = System.Windows.Application.Current.MainWindow
        }
            );
        }

        private string ExceptionToText(Exception ex, StringBuilder bob = null)
        {
            if (bob == null)
                bob = new StringBuilder();

            bob.AppendLine(ex.Message);
            bob.AppendLine(ex.StackTrace);

            if (ex.InnerException != null)
                ExceptionToText(ex.InnerException, bob);

            return bob.ToString();
        }

        protected override void OnStartup(StartupEventArgs e)
        {
            //using (var mgr = new UpdateManager("\\\\lmvm-fprsrv\\yhteinen\\InvoicingTool\\update"))
            //{
            //    mgr.UpdateApp();
            //}

            var main = new MainWindow();
            Navigator.NavigationService = main.NavigationFrame.NavigationService;
            main.Show();
        }

    }
}
