﻿using System.Windows.Controls;
using Telerik.Windows.Controls;

namespace InvoicingTool.UI.Views
{
    /// <summary>
    /// Interaction logic for Preparation.xaml
    /// </summary>
    public partial class Preparation : Page
    {
        public Preparation()
        {
            InitializeComponent();
        }

        private void DatePicker_OnParseDateTimeValue(object sender, ParseDateTimeEventArgs args)
        {
            if (!args.IsParsingSuccessful)
            {
                var date = args.TextToParse.ParseAsDate();
                if (date.HasValue)
                {
                    args.Result = date.Value;
                    args.IsParsingSuccessful = true;
                }
            }
        }
    }
}
