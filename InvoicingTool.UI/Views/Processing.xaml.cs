﻿using InvoicingTool.Business;
using InvoicingTool.Entities;
using InvoicingTool.UI.ViewModels;
using Microsoft.Win32;
using Squirrel;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Security.Principal;
using System.Windows;
using System.Windows.Controls;
using Telerik.Windows;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;
using Telerik.Windows.Documents.Model;
using Telerik.Windows.Persistence;
using Telerik.Windows.Persistence.Services;
using Telerik.Windows.Persistence.Storage;
using EditorCreatedEventArgs = Telerik.Windows.Controls.Data.DataFilter.EditorCreatedEventArgs;
using FilterOperatorsLoadingEventArgs = Telerik.Windows.Controls.Data.DataFilter.FilterOperatorsLoadingEventArgs;
using InputMode = Telerik.Windows.Controls.InputMode;

namespace InvoicingTool.UI.Views
{
    public partial class Processing : Page
    {

        public Processing()
        {
            InitializeComponent();
            ServiceProvider.RegisterPersistenceProvider<ICustomPropertyProvider>(typeof(RadDataFilter), new FilterDescriptorCustomPropertyProvider());
            ServiceProvider.RegisterPersistenceProvider<ICustomPropertyProvider>(typeof(RadGridView), new GridViewCustomPropertyProvider());
            DataFilter.Loaded += (s, e) => OnLoad(null, null);
            DataFilter.Unloaded += OnSave;
        }

        private void OnSave(object sender, RoutedEventArgs e)
        {
            var manager = new PersistenceManager();
            ViewModelLocator.Stream = manager.Save(DataFilter);
        }

        private void OnLoad(object sender, RoutedEventArgs e)
        {
            if (ViewModelLocator.Stream != null)
            {
                ViewModelLocator.Stream.Position = 0L;
                var manager = new PersistenceManager();
                manager.Load(DataFilter, ViewModelLocator.Stream);
            }
        }

        private void OnButtonSave(object sender, RoutedEventArgs e)
        {
            IsolatedStorageProvider isoProvider = new IsolatedStorageProvider();
            isoProvider.SaveToStorage("DataFilter");
        }

        private void OnButtonLoad(object sender, RoutedEventArgs e)
        {
            IsolatedStorageProvider isoProvider = new IsolatedStorageProvider();
            isoProvider.LoadFromStorage("DataFilter");
        }

        private void DataFilter_OnEditorCreated(object sender, EditorCreatedEventArgs e)
        {
            OnSave(null, null);

            switch (e.ItemPropertyDefinition.PropertyName)
            {
                case "Consolidation":
                    var consEditor = (RadComboBox) e.Editor;
                    consEditor.ItemsSource =
                        ViewModelLocator.Processing.OrderRows.Result.Select(x => x.Consolidation).Distinct();
                    break;

                case "OriginalCurrency":
                    var originalCurrencyEditor = (RadComboBox)e.Editor;
                    originalCurrencyEditor.ItemsSource =
                        ViewModelLocator.Processing.OrderRows.Result.Select(x => x.OriginalCurrency).Distinct();
                    break;

                case "CSR":
                    var csrEditor = (RadComboBox)e.Editor;
                    csrEditor.ItemsSource =
                        ViewModelLocator.Processing.OrderRows.Result.Select(x => x.CSR).Distinct();
                    break;

                case "InvoicingGroupDomestic":
                    var invDomesticEditor = (RadComboBox)e.Editor;
                    invDomesticEditor.ItemsSource =
                        ViewModelLocator.Processing.OrderRows.Result.Select(x => x.InvoicingGroupDomestic).Distinct();
                    break;

                case "InvoicingGroupForeign":
                    var invForeignEditor = (RadComboBox)e.Editor;
                    invForeignEditor.ItemsSource =
                        ViewModelLocator.Processing.OrderRows.Result.Select(x => x.InvoicingGroupForeign).Distinct();
                    break;

                //case "Note":
                //    var noteEditor = (RadRadioButton)e.Editor;
                //    break;

                case "OrderStatus":
                    var orderStatusEditor = (RadComboBox)e.Editor;
                    orderStatusEditor.ItemsSource =
                        ViewModelLocator.Processing.OrderRows.Result.Select(x => x.OrderStatus).Distinct();
                    break;

                case "PaymentTerm":
                    var paymentTermEditor = (RadComboBox)e.Editor;
                    paymentTermEditor.ItemsSource =
                        ViewModelLocator.Processing.OrderRows.Result.Select(x => x.PaymentTerm).Distinct();
                    break;

                case "PaymentMethod":
                    var paymentMethodEditor = (RadComboBox)e.Editor;
                    paymentMethodEditor.ItemsSource =
                        ViewModelLocator.Processing.OrderRows.Result.Select(x => x.PaymentMethod).Distinct();
                    break;

                case "PriceSource":
                    var priceSourceEditor = (RadComboBox)e.Editor;
                    priceSourceEditor.ItemsSource =
                        ViewModelLocator.Processing.OrderRows.Result.Select(x => x.PriceSource).Distinct();
                    break;

                case "IsForeign":
                    var foreignEditor = (RadComboBox)e.Editor;
                    foreignEditor.ItemsSource =
                        ViewModelLocator.Processing.OrderRows.Result.Select(x => x.IsForeign).Distinct();
                    break;

                case "GroupId":
                    var groupEditor = (RadComboBox)e.Editor;
                    groupEditor.ItemsSource =
                        ViewModelLocator.Processing.OrderRows.Result.Select(x => x.GroupId).Distinct();
                    break;

                case "Currency":
                    // This is a custom editor specified through the EditorTemplateSelector.
                    var currencyEditor = (RadComboBox)e.Editor;
                    currencyEditor.ItemsSource = ViewModelLocator.Processing.OrderRows.Result.Select(x => x.Currency).Distinct();
                    break;

                case "CustomerShip":
                    var customershipEditor = (RadComboBox)e.Editor;
                    customershipEditor.ItemsSource = Enum.GetNames(typeof(CustomerShip));
                    break;

                case "CustomerClass":
                    var mainClassEditor = (RadComboBox)e.Editor;
                    mainClassEditor.ItemsSource = ViewModelLocator.Processing.OrderRows.Result.Select(x => x.CustomerClass).Distinct();
                    break;

                case "CustomerGroup":
                    var mainGroupEditor = (RadComboBox)e.Editor;
                    mainGroupEditor.ItemsSource = ViewModelLocator.Processing.OrderRows.Result.Select(x => x.CustomerGroup).Distinct();
                    break;

                case "TitleFormat":
                    // This is a custom editor specified through the EditorTemplateSelector.
                    var formatEditor = (RadComboBox)e.Editor;
                    formatEditor.ItemsSource = ViewModelLocator.Processing.OrderRows.Result.Select(x => x.TitleFormat).Distinct();
                    break;

                case "TitleCategory2":
                    var category2Editor = (RadComboBox)e.Editor;
                    category2Editor.ItemsSource = ViewModelLocator.Processing.OrderRows.Result.Select(x => x.TitleCategory2).Distinct();
                    break;

                case "PeriodStart":
                    // This is a default editor.
                    var periodStartEditor = (RadDateTimePicker)e.Editor;
                    periodStartEditor.InputMode = InputMode.DatePicker;
                    break;

                case "PeriodEnd":
                    // This is a default editor.
                    var periodEndEditor = (RadDateTimePicker)e.Editor;
                    periodEndEditor.InputMode = InputMode.DatePicker;
                    break;

                case "Quantity":
                    // This is a custom editor specified through the EditorTemplateSelector.
                    var sliderEditor = (RadNumericUpDown)e.Editor;
                    sliderEditor.Minimum = 1;
                    sliderEditor.Maximum = 9999;
                    sliderEditor.SmallChange = 1;
                    break;
            }
        }

        private void MarkSelectedButton_Click(object sender, RoutedEventArgs e)
        {
            if (DataGrid.SelectedItems != null)
                foreach (var row in DataGrid.SelectedCells)
                    ((OrderRowViewModel)row.Item).Selected = true;
            e.Handled = true;
        }

        private void MarkAllButton_Click(object sender, RoutedEventArgs e)
        {
            if (DataGrid.Items == null) return;
            foreach (OrderRowViewModel item in DataGrid.Items)
                item.Selected = true;
            e.Handled = true;
        }

        private void UnmarkAll_Click(object sender, RoutedEventArgs e)
        {
            if (DataGrid.Items == null) return;
            foreach (OrderRowViewModel item in DataGrid.Items)
                item.Selected = false;
            e.Handled = true;
        }

        private void UnmarkSelectedButton_Click(object sender, RoutedEventArgs e)
        {
            if (DataGrid.SelectedItems != null)
                foreach (var row in DataGrid.SelectedCells)
                    ((OrderRowViewModel)row.Item).Selected = false;
            e.Handled = true;
        }

        private void DataFilter_FilterOperatorsLoading(object sender, FilterOperatorsLoadingEventArgs e)
        {
            var t = e.ItemPropertyDefinition.PropertyType;

            e.DefaultOperator = FilterOperator.IsEqualTo;

            if (t == typeof(String))
            {
                e.AvailableOperators.Remove(FilterOperator.IsGreaterThan);
                e.AvailableOperators.Remove(FilterOperator.IsGreaterThanOrEqualTo);
                e.AvailableOperators.Remove(FilterOperator.IsLessThan);
                e.AvailableOperators.Remove(FilterOperator.IsLessThanOrEqualTo);
                e.AvailableOperators.Remove(FilterOperator.IsNull);
                e.AvailableOperators.Remove(FilterOperator.IsNotNull);
            }

            if (t == typeof(CustomerShip))
            {
                e.AvailableOperators.Remove(FilterOperator.IsGreaterThan);
                e.AvailableOperators.Remove(FilterOperator.IsGreaterThanOrEqualTo);
                e.AvailableOperators.Remove(FilterOperator.IsLessThan);
                e.AvailableOperators.Remove(FilterOperator.IsLessThanOrEqualTo);
                e.AvailableOperators.Remove(FilterOperator.StartsWith);
                e.AvailableOperators.Remove(FilterOperator.EndsWith);
                e.AvailableOperators.Remove(FilterOperator.IsEmpty);
                e.AvailableOperators.Remove(FilterOperator.IsNotEmpty);
                e.AvailableOperators.Remove(FilterOperator.IsNull);
                e.AvailableOperators.Remove(FilterOperator.IsNotNull);
            }

            if (t == typeof(Decimal) || t == typeof(DateTime))
            {
                e.AvailableOperators.Remove(FilterOperator.StartsWith);
                e.AvailableOperators.Remove(FilterOperator.EndsWith);
                e.AvailableOperators.Remove(FilterOperator.IsEmpty);
                e.AvailableOperators.Remove(FilterOperator.IsNotEmpty);
            }
        }

        private void SetGroup_OnClick(object sender, RoutedEventArgs e)
        {
            var result = 0;
            RadWindow.Prompt("Group ID", (s, e2) => Int32.TryParse(e2.PromptResult, out result));
            foreach (var item in DataGrid.SelectedItems)
                ((OrderRowViewModel)item).GroupId = result;
        }

        private void NewGroup_OnClick(object sender, RoutedEventArgs e)
        {
            var groupId = ((ObservableCollection<OrderRowViewModel>)DataGrid.ItemsSource).Max(x => ((OrderRowViewModel)x).GroupId + 1);
            if (groupId == 0) return;
            foreach (var item in DataGrid.SelectedItems)
                ((OrderRowViewModel)item).GroupId = groupId;
        }

        private void ApplyConsolFee_Click(object sender, RoutedEventArgs e)
        {
            var vm = (ProcessingViewModel)DataContext;
            var list = DataGrid.SelectedItems.Select(x => (OrderRowViewModel)x).ToList();
            if (!list.Any()) return;

            foreach (var item in list)
            {
                if (vm.PercentageConsolidationFee)
                {
                    //item.ConsolidationFee = vm.ConsolFee * (item.GrossPrice + item.Airmail + item.Postage) / 100;
                    item.ConsolidationFee = vm.ConsolFee * (item.GrossPrice) / 100;
                }
                else
                {
                    if (vm.MultiplyConsolidationFee && item.TitleFrequency > 1)
                    {
                        item.ConsolidationFee = vm.ConsolFee * item.TitleFrequency;
                    }
                    else
                    {
                        item.ConsolidationFee = vm.ConsolFee;
                    }
                }
            }
            DataGrid.CalculateAggregates();
            DataGrid.GroupDescriptors.Reset();
        }

        private void ApplyDiscount_Click(object sender, RoutedEventArgs e)
        {
            var vm = (ProcessingViewModel) DataContext;
            var list = DataGrid.SelectedItems.Select(x => (OrderRowViewModel) x).ToList();
            if (!list.Any()) return;

            foreach (var item in list.Where(item => !item.Order.PriceFixed))
                item.DiscountPercent = vm.DiscountPercentage;

            DataGrid.CalculateAggregates();
            DataGrid.GroupDescriptors.Reset();
        }

        private void ButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            var vm = (ProcessingViewModel)DataContext;
            var list = DataGrid.SelectedItems.Select(x => (OrderRowViewModel)x).ToList();
            if (!list.Any()) return;

            var stubPricing = StubPeriodPricing.IsChecked;
            if (stubPricing.HasValue && stubPricing.Value)
            {
                foreach (var item in list)
                    item.AddToStubPeriods(vm.AdditionPercentage, vm.AdditionValue);
                return;
            }

            var publisherPricing = PublishersPricePricing.IsChecked;
            if (publisherPricing.HasValue && publisherPricing.Value)
            {
                foreach (var item in list.Where(x => x.GrossPrice > 0 && x.GrossPurchasePrice > 0))
                    item.AddToPublishersPrice(vm.AdditionPercentage, vm.AdditionValue);
                return;
            }

            var netPricing = NetPurchasePricing.IsChecked;
            if (netPricing.HasValue && netPricing.Value)
            {
                foreach (var item in list)
                    item.AddToNetPurchasePrice(vm.AdditionPercentage, vm.AdditionValue);
                return;
            }

            var customersSalesPricing = CustomerSalesPricing.IsChecked;
            if (!customersSalesPricing.HasValue || !customersSalesPricing.Value) return;

            foreach (var item in list)
                item.AddToCustomersSalesPrice(vm.AdditionPercentage, vm.AdditionValue);
        }
        
        private void LoadGridSettings_OnClick(object sender, RadRoutedEventArgs e)
        {
            try
            {
                IsolatedStorageProvider isoProvider = new IsolatedStorageProvider();
                isoProvider.LoadFromStorage("ProcessingGrid");
            }
            catch (Exception)
            {
                RadWindow.Alert("Could not load grid settings. The settings may have been corrupted or created using incompatible version.");
            }
        }

        private void SaveGridSettings_OnClick(object sender, RadRoutedEventArgs e)
        {
            try
            {
                IsolatedStorageProvider isoProvider = new IsolatedStorageProvider();
                isoProvider.SaveToStorage("ProcessingGrid");
            }
            catch (Exception)
            {
                RadWindow.Alert("Could not save grid settings. Please contact the support.");
            }
        }
        private void ClearGridSettings_OnClick(object sender, RadRoutedEventArgs e)
        {
            try
            {
                IsolatedStorageProvider isoProvider = new IsolatedStorageProvider();
                isoProvider.DeleteIsolatedStorageFiles();
            }
            catch (Exception)
            {
                RadWindow.Alert("Could not clear grid settings. Please contact the support.");
            }
        }
        private void ExportToExcel_OnClick(object sender, RadRoutedEventArgs e)
        {
            string extension = "xlsx";

            SaveFileDialog dialog = new SaveFileDialog()
            {
                DefaultExt = extension,
                Filter = String.Format("{1} files (*.{0})|*.{0}|All files (*.*)|*.*", extension, "Excel"),
                FilterIndex = 1
            };

            if (dialog.ShowDialog() == true)
            {
                using (Stream stream = dialog.OpenFile())
                {
                    DataGrid.ExportToXlsx(stream,
                        new GridViewDocumentExportOptions()
                        {
                            ShowColumnFooters = true,
                            ShowColumnHeaders = true,
                            ShowGroupFooters = true
                            //ExportDefaultStyles = true
                        });
                }
            }
        }
        /*private void ExportToPDF_OnClick(object sender, RadRoutedEventArgs e)
        {
            string extension = "pdf";

            SaveFileDialog dialog = new SaveFileDialog()
            {
                DefaultExt = extension,
                Filter = String.Format("{1} files (*.{0})|*.{0}|All files (*.*)|*.*", extension, "Pdf"),
                FilterIndex = 1
            };

            if (dialog.ShowDialog() == true)
            {
                using (Stream stream = dialog.OpenFile())
                {
                    DataGrid.ExportToPdf(stream,
                        new GridViewPdfExportOptions()
                        {
                            ShowColumnFooters = true,
                            ShowColumnHeaders = true,
                            ShowGroupFooters = true,
                            PageOrientation = PageOrientation.Landscape,
                            AutoFitColumnsWidth = true
                        });
                }
            }
        }*/
        private void OpenLSOrder_OnClick(object sender, RadRoutedEventArgs e)
        {
            var order = DataGrid.CurrentItem as OrderRowViewModel;
            if (order != null)
            {
                Process.Start(String.Format("libsys:TILAUS/{0}", order.OrderRowNumber));
            }
        }

        private void EditNote_OnClick(object sender, RadRoutedEventArgs e)
        {
            var item = DataGrid.SelectedItem as OrderRowViewModel;
            if (item == null) return;

            DialogParameters parameters = new DialogParameters();
            parameters.Owner = Application.Current.MainWindow;
            string teksti = null;
           
            parameters.Header = "Invoicing Tool";
            parameters.Content = "Edit Customer Note:";
            parameters.DefaultPromptResultValue = item.NoteText;
            parameters.Closed = (s, ev) => teksti = ev.PromptResult; 

            RadWindow.Prompt(parameters);
            if (teksti != null)
            {
                var windowsIdentity = WindowsIdentity.GetCurrent();
                string pvm = DateTime.Now.ToString("dd.MM.yyyy");
                //string aika = DateTime.Now.ToString("HH:mm");
                string nimi = "";
                if (windowsIdentity != null)
                {
                    nimi = windowsIdentity.Name;
                    nimi = nimi.Split('\\')[1];
                }

                if (teksti.Length > 0)
                    teksti += " [" + nimi + ", " + pvm + "]";

                if (InvoicingToolComponent.UpdateNote(item.MainCustomerId, teksti))
                {
                    item.NoteText = teksti;

                    foreach (OrderRowViewModel orderRowItem in DataGrid.Items)
                    {
                        if (orderRowItem.MainCustomerId == item.MainCustomerId) orderRowItem.NoteText = teksti;
                    }
                }
                else
                {
                    RadWindow.Alert("Could not update the note.");
                }
            }
        }

        private void RowMenu_OnOpened(object sender, RoutedEventArgs e)
        {
            var menu = (RadContextMenu)sender;
            var cell = menu.GetClickedElement<GridViewCell>();
            if (cell == null)
            {
                return;
            }

            DataGrid.SelectedItems.Clear();
            cell.ParentRow.IsSelected = true;
            cell.ParentRow.IsCurrent = true;
            cell.IsCurrent = true;
        }

        private void Copy_OnClick(object sender, RadRoutedEventArgs e)
        {
            if (DataGrid.CurrentCell != null)
            {
                Clipboard.SetText(DataGrid.CurrentCell.Value.ToString());
            }
        }

        private void ReloadItem_OnClick(object sender, RadRoutedEventArgs e)
        {
            var item = DataGrid.SelectedItem as OrderRowViewModel;
            if (item == null)
            {
                return;
            }

            item.ReloadAsync();
        }

        private void ReloadAll_OnClick(object sender, RadRoutedEventArgs e)
        {
            ViewModelLocator.Processing.ReloadAll();
        }

        private void expander_Loaded(object sender, RoutedEventArgs e)
        {
            RadExpander expander = sender as RadExpander;
            if (expander != null)
            {
                RadToggleButton button = expander.ChildrenOfType<RadToggleButton>().FirstOrDefault();
                button.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;
            }
        }

        private async void CheckUpdates_ClickAsync(object sender, RoutedEventArgs e)
        {
            var polku = "\\\\lmvm-fprsrv\\yhteinen\\InvoicingTool\\update";
            //var polku = "C:\\Temp\\update";

            using (var mgr = new UpdateManager(polku))
            {
                UpdateInfo updateInfo = await mgr.CheckForUpdate();
                if (updateInfo.ReleasesToApply.Any())
                {
                    var newVersion = updateInfo.FutureReleaseEntry.Version;
                    string releaseInfo;
                    try
                    {
                        releaseInfo = updateInfo.FutureReleaseEntry.GetReleaseNotes(polku);
                        releaseInfo = releaseInfo.Replace("<![CDATA[", "");
                        releaseInfo = releaseInfo.Replace("<p>", "");
                        releaseInfo = releaseInfo.Replace("</p>", "");
                        releaseInfo = releaseInfo.Replace("]]>", "");
                        releaseInfo = releaseInfo.Replace("\n", "");
                    }
                    catch (Exception)
                    {
                        releaseInfo = "";
                    }
                    var confirmed = false;

                    string promptText = "Found new version (" + newVersion + "), proceed with downloading the update?";
                    if (!String.IsNullOrEmpty(releaseInfo))
                    {
                        promptText += Environment.NewLine + "Version info:" + Environment.NewLine + releaseInfo;
                    }

                    DialogParameters parameters = new DialogParameters
                    {
                        Owner = Application.Current.MainWindow,
                        Content = promptText,
                        Closed = (senderi, args) => confirmed = args.DialogResult.HasValue && args.DialogResult.Value
                    };
                    RadWindow.Confirm(parameters);

                    if (!confirmed)
                    {
                        return;
                    }

                    await mgr.UpdateApp();
                    DialogParameters paramssit = new DialogParameters
                    {
                        Owner = Application.Current.MainWindow,
                        Content = "New version has been downloaded and will be installed on the next startup.",
                        Header = "Update"
                    };
                    RadWindow.Alert(paramssit);
                }
                else
                {
                    DialogParameters parameters = new DialogParameters
                    {
                        Owner = Application.Current.MainWindow,
                        Content = "There are no updates available.",
                        Header = "Update"
                    };
                    RadWindow.Alert(parameters);
                }
            }
        }

        private void LoadPurchaseInvoicePaidDates_Click(object sender, RadRoutedEventArgs e)
        {
            ViewModelLocator.Processing.ReloadAll(true);
        }
    }

}
