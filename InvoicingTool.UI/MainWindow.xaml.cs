﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Deployment.Application;
using System.Reflection;
using System;

namespace InvoicingTool.UI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            var versio = getRunningVersion();
            mainWindow.Title += " v" + versio.Major + "." + versio.Minor + "." + versio.Build;
            Loaded += (s, e) =>
            {
                Navigator.NavigationService.Navigate(Navigator.PreparationView);
                RemoveHotKeysFromFrmContent(NavigationFrame);
            };
        }

        private Version getRunningVersion()
        {
            try
            {
                return ApplicationDeployment.CurrentDeployment.CurrentVersion;
            }
            catch (Exception)
            {
                return Assembly.GetExecutingAssembly().GetName().Version;
            }
        }

        public static void RemoveHotKeysFromFrmContent(Frame frmContent)
        {
            frmContent.CommandBindings.Add(new CommandBinding(NavigationCommands.BrowseBack, EmptyEvent));
            frmContent.CommandBindings.Add(new CommandBinding(NavigationCommands.Refresh, EmptyEvent));
            frmContent.CommandBindings.Add(new CommandBinding(NavigationCommands.BrowseForward, EmptyEvent));
            frmContent.CommandBindings.Add(new CommandBinding(NavigationCommands.BrowseHome, EmptyEvent));
            frmContent.CommandBindings.Add(new CommandBinding(NavigationCommands.BrowseStop, EmptyEvent));
            frmContent.CommandBindings.Add(new CommandBinding(NavigationCommands.DecreaseZoom, EmptyEvent));
            frmContent.CommandBindings.Add(new CommandBinding(NavigationCommands.Favorites, EmptyEvent));
            frmContent.CommandBindings.Add(new CommandBinding(NavigationCommands.FirstPage, EmptyEvent));
            frmContent.CommandBindings.Add(new CommandBinding(NavigationCommands.GoToPage, EmptyEvent));
            frmContent.CommandBindings.Add(new CommandBinding(NavigationCommands.IncreaseZoom, EmptyEvent));
            frmContent.CommandBindings.Add(new CommandBinding(NavigationCommands.LastPage, EmptyEvent));
            frmContent.CommandBindings.Add(new CommandBinding(NavigationCommands.NavigateJournal, EmptyEvent));
            frmContent.CommandBindings.Add(new CommandBinding(NavigationCommands.NextPage, EmptyEvent));
            frmContent.CommandBindings.Add(new CommandBinding(NavigationCommands.PreviousPage, EmptyEvent));
            frmContent.CommandBindings.Add(new CommandBinding(NavigationCommands.Search, EmptyEvent));
            frmContent.CommandBindings.Add(new CommandBinding(NavigationCommands.Zoom, EmptyEvent));
        }

        static void EmptyEvent(object sender, ExecutedRoutedEventArgs args)
        {
            
        }
    }
}
