﻿namespace InvoicingTool.Entities
{
    public class CustomerGroup
    {
        public int? Id { get; set; }
        public string Name { get; set; }
    }
}
