﻿using System;
using System.Collections.Generic;

namespace InvoicingTool.Entities
{
    public class ProformaInvoice
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Creator { get; set; }
        public DateTime Created { get; set; }
        public IEnumerable<string> OrderRows { get; set; }

        public override string ToString()
        {
            return String.Format("{0} - {1} - {2}", Name, Created.ToShortDateString(), Creator.Replace("LEHTIM\\",""));
        }
    }
}
