﻿namespace InvoicingTool.Entities
{
    public class InvoicingGroup
    {
        public int? Id { get; set; }
        public string Name { get; set; }
    }
}
