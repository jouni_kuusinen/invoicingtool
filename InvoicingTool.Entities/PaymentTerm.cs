﻿namespace InvoicingTool.Entities
{
    public class PaymentTerm
    {
        public int? Id { get; set; }
        public string Name { get; set; }
    }
}
