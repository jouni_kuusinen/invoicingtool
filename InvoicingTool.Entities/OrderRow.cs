﻿using System;

namespace InvoicingTool.Entities
{
    public class OrderRow
    {
        public OrderRow()
        {
            Title = new Title();
        }

        public string OrderRowId { get; set; }
        public string OrderId { get; set; }
        public string OrderNumber { get; set; }
        public Title Title { get; set; }

        public string PublisherName { get; set; }

        public string MainCustomerId { get; set; }
        public string InvoiceCustomerId { get; set; }
        public string DeliveryCustomerId { get; set; }

        public string DeliveryCustomer { get; set; }
        public string InvoiceCustomer { get; set; }
        public string MainCustomer { get; set; }

        public string PaymentTerm { get; set; }
        public string InvoicingGroupDomestic { get; set; }
        public string InvoicingGroupForeign { get; set; }
        public string CustomerGroup { get; set; }
        public string CustomerClass { get; set; }
        public CustomerShip CustomerShip { get; set; }
        
        public string Status { get; set; }
        public string Currency { get; set; }
        public string PriceSource { get; set; }
        public DateTime? PurchasePaid { get; set; }
        public bool IsForeign { get; set; } 

        public int PriceYear { get; set; }
        public int Quantity { get; set; }
        public int GroupId { get; set; }
        public int Months { get; set; }

        public DateTime PriceValidFrom { get; set; }
        public DateTime PeriodStart { get; set; }
        public DateTime PeriodEnd { get; set; }
        public DateTime? InvoicingDelayedUntil { get; set; }
        public DateTime? LastInvoiceDate { get; set; }

        public decimal GrossMargin { get; set; }
        public decimal NetPrice { get; set; }
        public decimal GrossPrice { get; set; }
        public decimal NetPurchasePrice { get; set; }
        public decimal GrossPurchasePrice { get; set; }
        public decimal Postage { get; set; }
        public decimal Airmail { get; set; }
        public decimal DiscountPercent { get; set; }
        public decimal VatPercent { get; set; }
        public decimal ConsolidationFee { get; set; }
        public string Comment1 { get; set; }
        public string Comment2 { get; set; }

        public string Consolidation { get; set; }
        public string CustomerOrderNumber { get; set; }
        public string CostCenter { get; set; }

        // status
        public bool Renewed { get; set; }
        public bool TransferOrder { get; set; }
        public bool UnconfirmedRenewal { get; set; }
        public bool OrderSent { get; set; }
        public bool PriceFixed { get; set; }
        public bool Activated { get; set; }
        public bool InvoicePending { get; set; }
        public bool Invoiced { get; set; }
        public bool InvoiceLater { get; set; }
        public bool AdditionalInvoiced { get; set; }
        public bool Credited { get; set; }
        public bool Canceled { get; set; }
        public bool NoRenewal { get; set; }
        public bool LibNetOrder { get; set; }
        public bool NegativeMargin { get; set; }
        public bool ExceptionalPeriod { get; set; }
        public bool ModifiedPrice { get; set; }
        public bool ManualInvoicing { get; set; }
        public bool OnHold { get; set; }
        //
        public int ProductGroupID { get; set; }
        public string ProductGroup { get; set; }
        public decimal? VatDivision { get; set; }
        public bool SeriesTitle { get; set; }
        public string CSR { get; set; }
        public string MainTitleStatus { get; set; }
        public string OrderStatus { get; set; }
        public bool NotViaAgents { get; set; }
        public string OriginalCurrency { get; set; }
        public decimal? CustomerCurrencyRate { get; set; }
        public bool Note { get; set; }
        public string NoteText { get; set; }
        public string LMS { get; set; }
        public bool FixedPrices { get; set; }
        public string PurchaseInvoiceNumber { get; set; }
        public string StaticOrderNumber { get; set; }
        public string PaymentMethod { get; set; }
        public bool Medical { get; set; }
    }
}
