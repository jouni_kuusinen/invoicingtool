﻿using System;

namespace InvoicingTool.Entities
{
    public class Customer
    {

        public string CustomerId { get; set; }
        public string CustomerName { get; set; }

        public override string ToString()
        {
            return String.Format("{0} {1}", CustomerId, CustomerName);
        }
    }
}
