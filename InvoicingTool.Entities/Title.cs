﻿
namespace InvoicingTool.Entities
{
    public class Title
    {

        public string TitleId { get; set; }
        public string TitleName { get; set; }
        public string TitleCode { get; set; }
        public string Format { get; set; }
        public string Category { get; set; }
        public string Category2 { get; set; }

        public int Frequency { get; set; }
        public bool CustomizedPrice { get; set; }

        public override string ToString()
        {
            return TitleName;
        }
    }
}
