﻿using System;

namespace InvoicingTool.Entities
{

    public class SearchCriterion
    {

        public SearchCriterion(int? mainCustomerId, int? invoiceCustomerId, int? deliveryCustomerId, CustomerShip customerShip, DateTime? startDate, DateTime? endDate, CustomerClass customerClass, CustomerGroup customerGroup, InvoicingGroup invoicingGroupDomestic, InvoicingGroup invoicingGroupForeign)
        {
            MainCustomerId = mainCustomerId;
            InvoiceCustomerId = invoiceCustomerId;
            DeliveryCustomerId = deliveryCustomerId;
            CustomerShip = customerShip;
            StartDate = startDate;
            EndDate = endDate;
            CustomerClass = customerClass;
            CustomerGroup = customerGroup;
            InvoicingGroupDomestic = invoicingGroupDomestic;
            InvoicingGroupForeign = invoicingGroupForeign;
        }

        public int? MainCustomerId { get; set; }
        public int? InvoiceCustomerId { get; set; }
        public int? DeliveryCustomerId { get; set; }
        public CustomerShip CustomerShip { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public CustomerClass CustomerClass { get; set; }
        public CustomerGroup CustomerGroup { get; set; }
        public InvoicingGroup InvoicingGroupDomestic { get; set; }
        public InvoicingGroup InvoicingGroupForeign { get; set; }
    }
}
