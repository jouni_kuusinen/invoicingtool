﻿using System.Linq;
using InvoicingTool.Data;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using InvoicingTool.Business;

namespace InvoicingTool.Tests
{
    [TestClass]
    public class DacTests
    {
        
        // confirm order row currently "untestable" in production enviroment, therefore no test exists.

        [TestMethod]
        public void TestGetOrderRow()
        {
            const string orderRowId = "";
            var order = DataDAC.SelectOrderRowById(orderRowId, Entities.CustomerShip.Finland);
            Assert.AreEqual(order.OrderRowId, orderRowId);
        }

        [TestMethod]
        public void TestGetUserId()
        {
            const string userName = "marjahi";
            var id = DataDAC.GetUserId(userName);
            Assert.AreEqual(id, 106);

            const string userName2 = "jakke";
            var id2 = DataDAC.GetUserId(userName2);
            Assert.AreEqual(id2, 10017);
        }

        [TestMethod]
        public void TestGetInvoicingGroups()
        {
            var groups = InvoicingToolComponent.GetInvoicingGroups();
            Assert.IsTrue(groups.Any());
            var firstGroup = groups.FirstOrDefault(x => x.Id == 1);
            Assert.IsTrue(firstGroup != null);
            Assert.AreEqual(firstGroup.Name, "Monthly", true);
            Assert.IsTrue(groups.Count() >= 7);
        }

        [TestMethod]
        public void TestGetCustomerClasses()
        {
            var classes = InvoicingToolComponent.GetCustomerClasses();
            Assert.IsTrue(classes.Any());
            var firstClass = classes.FirstOrDefault(x => x.Id == 1);
            Assert.IsTrue(firstClass != null);
            Assert.AreEqual(firstClass.Name, "Corporate", true);
            Assert.IsTrue(classes.Count() >= 9);
        }

        [TestMethod]
        public void TestGetCustomerGroups()
        {
            var groups = InvoicingToolComponent.GetCustomerGroups();
            Assert.IsTrue(groups.Any());
            var firstGroup = groups.FirstOrDefault(x => x.Id == 1);
            Assert.IsTrue(firstGroup != null);
            Assert.AreEqual(firstGroup.Name, "HANSEL", true);
            Assert.IsTrue(groups.Count() >= 26);
        }

        [TestMethod]
        public void TestGetPaymentTerms()
        {
            var terms = InvoicingToolComponent.GetPaymentTerms();
            Assert.IsTrue(terms.Any());
            var firstTerm = terms.FirstOrDefault(x => x.Id == 1);
            Assert.IsTrue(firstTerm != null);
            Assert.AreEqual(firstTerm.Name, "14 days", true);
            Assert.IsTrue(terms.Count() >= 7);
        }

        [TestMethod]
        public void TestGetLatestInvoiceRuns() // will work only with testdata
        {
            var invoiceRuns = InvoicingToolComponent.GetLatestInvoiceRuns();
            Assert.IsTrue(invoiceRuns.Any());
            var firstRun = invoiceRuns.FirstOrDefault(x => x.Id == 1);
            Assert.IsTrue(firstRun != null);
            Assert.AreEqual(firstRun.Creator, "LEHTIM\\jakke", true);
        }

        [TestMethod]
        public void TestGetInvoiceRunOrders()
        {
            var orders = InvoicingToolComponent.GetInvoiceRunOrders(1);
            Assert.IsTrue(orders.Any());
            var firstOrder = orders.FirstOrDefault(x => x.OrderRowId == "2412612");
            Assert.IsTrue(firstOrder != null);
            Assert.AreEqual(firstOrder.Title.TitleId, "37049", true);
        }
    }
}
