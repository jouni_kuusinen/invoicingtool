﻿using System;
using System.Linq;
using OrderRowDAC = InvoicingTool.Data.OrderRowDAC;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace InvoicingTool.Tests.Data
{
    [TestClass]
    public class TestOrderRowDAC
    {
        [TestMethod]
        public void TestSelectOrdersReadyForInvoicing()
        {
            var x = OrderRowDAC.SelectOrdersReadyForInvoicing2();
            Assert.IsTrue(x.Any());

            var first = x.FirstOrDefault();
            Assert.IsNotNull(first);
            Assert.IsNotNull(first.Title);
            Assert.IsNotNull(first.Title.Format);
        }
    }
}
