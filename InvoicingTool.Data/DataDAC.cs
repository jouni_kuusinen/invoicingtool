﻿using InvoicingTool.Data.Properties;
using InvoicingTool.Entities;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Security.Principal;

namespace InvoicingTool.Data
{
    public class DataDAC : DataAccessComponent
    {
        static DataDAC()
        {
            DatabaseFactory.SetDatabaseProviderFactory(new DatabaseProviderFactory(), false);
        }

        public static OrderRow SelectOrderRowById(string orderRowId, CustomerShip customership)
        {
            var sqlStatement = Settings.Default.SqlOrderRowColumns + Settings.Default.SqlSelectOrderRowById;
            var db = DatabaseFactory.CreateDatabase(ConnectionName);
            var order = new OrderRow();

            using (var cmd = db.GetSqlStringCommand(sqlStatement))
            {
                cmd.CommandTimeout = 600;
                db.AddInParameter(cmd, "@CustomershipId", DbType.Int32, customership);
                db.AddInParameter(cmd, "@OrderRowId", DbType.String, orderRowId);
                using (var dr = db.ExecuteReader(cmd))
                {
                    dr.Read();
                    ReadOrderRow(dr, order);
                }
            }

            return order;
        }

        public static OrderRow SelectOrderRowById(OrderRow original, bool onlypurchases = false)
        {
            string sqlStatement = "";

            if (onlypurchases)
            {
                sqlStatement = Settings.Default.SqlSelectOrderRowByIdOnlyPurchases;
            }
            else
            {
                sqlStatement = Settings.Default.SqlSelectOrderRowByIdLite;
            }
            
            var db = DatabaseFactory.CreateDatabase(ConnectionName);

            using (var cmd = db.GetSqlStringCommand(sqlStatement))
            {
                cmd.CommandTimeout = 600;
                db.AddInParameter(cmd, "@CustomershipId", DbType.Int32, original.CustomerShip);
                db.AddInParameter(cmd, "@OrderRowId", DbType.String, original.OrderRowId);
                using (var dr = db.ExecuteReader(cmd))
                {
                    dr.Read();
                    if (onlypurchases)
                    {
                        original.PurchasePaid = GetDataValue<DateTime?>(dr, "PurchasePaid");
                    }
                    else
                    {
                        original.LastInvoiceDate = GetDataValue<DateTime?>(dr, "LastInvoiceDate");
                        original.CustomerCurrencyRate = GetDataValue<decimal?>(dr, "CustomerCurrencyRate");
                        original.OriginalCurrency = GetDataValue<string>(dr, "OriginalCurrency");
                        original.PurchaseInvoiceNumber = GetDataValue<string>(dr, "PurchaseInvoiceNumber");
                    } 
                }
            }

            return original;
        }

        public static Collection<CustomerGroup> SelectCustomerGroups()
        {
            var sqlStatement = Settings.Default.SqlSelectCustomerGroups;
            var db = DatabaseFactory.CreateDatabase(ConnectionName);
            var list = new Collection<CustomerGroup>
            {
                new CustomerGroup
                {
                    Id = null,
                    Name = "All"
                }
            };

            using (var cmd = db.GetSqlStringCommand(sqlStatement))
            {
                cmd.CommandTimeout = 600;
                using (var dr = db.ExecuteReader(cmd))
                {
                    while (dr.Read())
                    {
                        list.Add(new CustomerGroup
                        {
                            Id = GetDataValue<int>(dr, "Id"),
                            Name = GetDataValue<string>(dr, "Name")
                        });
                    }
                }
            }

            return list;
        }

        /// <summary>
        /// Returns a list of current payment terms
        /// </summary>
        /// <returns></returns>
        public static Collection<PaymentTerm> SelectPaymentTerms()
        {
            var sqlStatement = Settings.Default.SqlSelectPaymentTerms;
            var db = DatabaseFactory.CreateDatabase(ConnectionName);
            var list = new Collection<PaymentTerm>
            {
                new PaymentTerm
                {
                    Id = null,
                    Name = "All"
                }
            };

            using (var cmd = db.GetSqlStringCommand(sqlStatement))
            {
                cmd.CommandTimeout = 600;
                using (var dr = db.ExecuteReader(cmd))
                {
                    while (dr.Read())
                    {
                        list.Add(new PaymentTerm
                        {
                            Id = GetDataValue<int>(dr, "Id"),
                            Name = GetDataValue<string>(dr, "Name")
                        });
                    }
                }
            }

            return list;
        }

        /// <summary>
        /// Returns a list of current invoicing groups
        /// </summary>
        /// <returns></returns>
        public static Collection<InvoicingGroup> SelectInvoicingGroups()
        {
            var sqlStatement = Settings.Default.SqlSelectInvoicingGroups;
            var db = DatabaseFactory.CreateDatabase(ConnectionName);
            var list = new Collection<InvoicingGroup>
            {
                new InvoicingGroup
                {
                    Id = null,
                    Name = "All"
                }
            };

            using (var cmd = db.GetSqlStringCommand(sqlStatement))
            {
                cmd.CommandTimeout = 600;
                using (var dr = db.ExecuteReader(cmd))
                {
                    while (dr.Read())
                    {
                        list.Add(new InvoicingGroup
                        {
                            Id = GetDataValue<int>(dr, "Id"),
                            Name = GetDataValue<string>(dr, "Name")
                        });
                    }
                }
            }

            return list;
        }

        /// <summary>
        /// Returns a list of current customer classes
        /// </summary>
        /// <returns></returns>
        public static Collection<CustomerClass> SelectCustomerClasses()
        {
            var sqlStatement = Settings.Default.SqlSelectCustomerClasses;
            var db = DatabaseFactory.CreateDatabase(ConnectionName);
            var list = new Collection<CustomerClass>
            {
                new CustomerClass
                {
                    Id = null,
                    Name = "All"
                }
            };

            using (var cmd = db.GetSqlStringCommand(sqlStatement))
            {
                cmd.CommandTimeout = 600;
                using (var dr = db.ExecuteReader(cmd))
                {
                    while (dr.Read())
                    {
                        list.Add(new CustomerClass
                        {
                            Id = GetDataValue<int?>(dr, "Id"),
                            Name = GetDataValue<string>(dr, "Name")
                        });
                    }
                }
            }

            return list;
        }

        /// <summary>
        /// Retrieves all currently applicable orders for invoicing based on parameters from user input.
        /// </summary>
        /// <returns></returns>
        public static Collection<OrderRow> SelectOrdersReadyForInvoicing(SearchCriterion criterion)
        {
            var sqlStatement = Settings.Default.SqlOrderRowColumnsLite;
            var db = DatabaseFactory.CreateDatabase(ConnectionName);
            var orderRows = new Collection<OrderRow>();

            using (var cmd = db.GetSqlStringCommand(sqlStatement))
            {
                cmd.CommandTimeout = 600;
                db.AddInParameter(cmd, "@MainCustomerId", DbType.Int32, criterion.MainCustomerId);
                db.AddInParameter(cmd, "@InvoiceCustomerId", DbType.Int32, criterion.InvoiceCustomerId);
                db.AddInParameter(cmd, "@DeliveryCustomerId", DbType.Int32, criterion.DeliveryCustomerId);
                db.AddInParameter(cmd, "@CustomershipId", DbType.Int32, criterion.CustomerShip);

                if (criterion.StartDate == null)
                {
                    db.AddInParameter(cmd, "@StartDate", DbType.DateTime, DBNull.Value);
                }
                else
                {
                    db.AddInParameter(cmd, "@StartDate", DbType.DateTime, criterion.StartDate);
                }

                if (criterion.EndDate == null)
                {
                    db.AddInParameter(cmd, "@EndDate", DbType.DateTime, DBNull.Value);
                }
                else
                {
                    db.AddInParameter(cmd, "@EndDate", DbType.DateTime, criterion.EndDate);
                }

                db.AddInParameter(cmd, "@CustomerClassId", DbType.Int32, criterion.CustomerClass.Id);
                db.AddInParameter(cmd, "@CustomerGroupId", DbType.Int32, criterion.CustomerGroup.Id);
                db.AddInParameter(cmd, "@InvoicingGroupDomesticId", DbType.Int32, criterion.InvoicingGroupDomestic.Id);
                db.AddInParameter(cmd, "@InvoicingGroupForeignId", DbType.Int32, criterion.InvoicingGroupForeign.Id);

                using (var dr = db.ExecuteReader(cmd))
                {
                    while (dr.Read())
                    {
                        var orderRow = new OrderRow();
                        ReadOrderRow(dr, orderRow);
                        orderRows.Add(orderRow);
                    }
                }
            }

            return orderRows;
        }

        /// <summary>
        /// Push order to invoicing and make according steps (update prices, status, etc.)
        /// </summary>
        /// <param name="orderRow"></param>
        /// <param name="invoice">1 = To invoicing, 0 = Return status</param>
        /// <returns></returns>
        public static bool ConfirmOrderRow(OrderRow orderRow, bool invoice)
        {
            var windowsIdentity = WindowsIdentity.GetCurrent();
            if (windowsIdentity != null)
            {
                var userId = GetUserId(windowsIdentity.Name);

                var db = DatabaseFactory.CreateDatabase(ConnectionName);
                using (var cmd = db.GetStoredProcCommand("procUpdateTilausriviLaskutusKuittausUusi"))
                {
                    cmd.CommandTimeout = 600;
                    db.AddInParameter(cmd, "intTilausriviID", DbType.Int32, Int32.Parse(orderRow.OrderRowId));
                    db.AddInParameter(cmd, "decBruttoMyyntihinta", DbType.Decimal, orderRow.GrossPrice);
                    db.AddInParameter(cmd, "decAlennusprosentti", DbType.Decimal, orderRow.DiscountPercent);
                    db.AddInParameter(cmd, "decPostimaksu", DbType.Decimal, orderRow.Postage);
                    db.AddInParameter(cmd, "decLentopostimaksu", DbType.Decimal, orderRow.Airmail);
                    db.AddInParameter(cmd, "strValuutta", DbType.String, orderRow.Currency);
                    db.AddInParameter(cmd, "intHenkiloID", DbType.Int32, userId);
                    db.AddInParameter(cmd, "intLaskutusryhmittely", DbType.Int32, orderRow.GroupId);
                    db.AddInParameter(cmd, "Laskutukseen", DbType.Boolean, invoice);
                    db.AddInParameter(cmd, "decKonsolidointimaksu", DbType.Decimal, orderRow.ConsolidationFee);
                    try
                    {
                        var rowsAffected = db.ExecuteNonQuery(cmd);
                        return rowsAffected == 1;
                    }
                    catch (Exception)
                    {
                        Console.WriteLine("Failed to execute ConfirmOrderRow on" + orderRow.OrderRowId);
                        return false;
                    }
                }
            }
            else
            {
                return false;
            }
        }

        public static ProformaInvoice CreateProformaInvoice(IEnumerable<string> orderRowIds, string name)
        {
            var db = DatabaseFactory.CreateDatabase(ConnectionName);
            var date = DateTime.Now;
            var windowsIdentity = WindowsIdentity.GetCurrent();
            if (windowsIdentity != null)
            {
                var user = windowsIdentity.Name;

                var sqlStatement = Settings.Default.SqlInsertProformaInvoice;
                using (var cmd = db.GetSqlStringCommand(sqlStatement))
                {
                    cmd.CommandTimeout = 600;
                    db.AddInParameter(cmd, "@Name", DbType.String, name);
                    db.AddInParameter(cmd, "@Creator", DbType.String, user);
                    db.AddInParameter(cmd, "@Date", DbType.DateTime, date);
                    var rowsAffected = db.ExecuteNonQuery(cmd);
                    if (rowsAffected != 1) 
                        throw new InvalidOperationException("Unexpected result from create proforma invoice: " + rowsAffected);
                }

                var sqlStatement3 = Settings.Default.SqlGetProformaInvoiceByParams;
                var invoice = new ProformaInvoice();
                using (var cmd = db.GetSqlStringCommand(sqlStatement3))
                {
                    cmd.CommandTimeout = 600;
                    db.AddInParameter(cmd, "@Name", DbType.String, name);
                    db.AddInParameter(cmd, "@Created", DbType.DateTime, date);
                    db.AddInParameter(cmd, "@Creator", DbType.String, user);

                    using (var dr = db.ExecuteReader(cmd))
                    {
                        while (dr.Read())
                        {
                            invoice.Id = GetDataValue<int>(dr, "Id");
                            invoice.Name = GetDataValue<string>(dr, "Name");
                            invoice.Created = GetDataValue<DateTime>(dr, "Created");
                            invoice.Creator = GetDataValue<string>(dr, "Creator");
                        }
                    }
                }

                var sqlStatement2 = Settings.Default.SqlInsertProformaInvoiceRow;
                foreach (var id in orderRowIds)
                {
                    using (var cmd = db.GetSqlStringCommand(sqlStatement2))
                    {
                        cmd.CommandTimeout = 600;
                        db.AddInParameter(cmd, "@ProformaInvoiceId", DbType.Int32, invoice.Id);
                        db.AddInParameter(cmd, "@OrderRowId", DbType.Int32, id);
                        var rowsAffected = db.ExecuteNonQuery(cmd);
                        if (rowsAffected != 1)
                            throw new InvalidOperationException("Unexpected result from create proforma invoice rows: " + rowsAffected);
                    }                
                }

                return invoice;
            }
            else
            {
                return null;
            }
        }

        public static Boolean UpdateNote(string id, string text)
        {
            int rows = 0;
            try
            {
                var sqlStatement = Settings.Default.SqlUpdateNote;
                var db = DatabaseFactory.CreateDatabase(ConnectionName);

                using (var cmd = db.GetSqlStringCommand(sqlStatement))
                {
                    cmd.CommandTimeout = 600;
                    db.AddInParameter(cmd, "@id", DbType.Int32, id);
                    db.AddInParameter(cmd, "@text", DbType.String, text);
                    rows = db.ExecuteNonQuery(cmd);
                }
                return (rows > 0);
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static Boolean DeleteProforma(int id)
        {
            int rows = 0;
            try
            {
                var sqlStatement = Settings.Default.SqlDeleteProforma;
                var db = DatabaseFactory.CreateDatabase(ConnectionName);

                using (var cmd = db.GetSqlStringCommand(sqlStatement))
                {
                    cmd.CommandTimeout = 600;
                    db.AddInParameter(cmd, "@id", DbType.Int32, id);
                    rows = db.ExecuteNonQuery(cmd);
                }
                return (rows > 0);
            }
            catch (Exception)
            {
                return false;
            }
        }
        /// <summary>
        /// Return latest invoicing runs (proforma)
        /// </summary>
        /// <returns></returns>
        public static Collection<ProformaInvoice> GetLatestInvoiceRuns()
        {
            try
            {
                var sqlStatement = Settings.Default.SqlGetProformaInvoiceRuns;
                var db = DatabaseFactory.CreateDatabase(ConnectionName);
                var list = new Collection<ProformaInvoice>();

                using (var cmd = db.GetSqlStringCommand(sqlStatement))
                {
                    cmd.CommandTimeout = 600;
                    using (var dr = db.ExecuteReader(cmd))
                    {
                        while (dr.Read())
                        {
                            var invoice = new ProformaInvoice()
                            {
                                Id = GetDataValue<int>(dr, "Id"),
                                Created = GetDataValue<DateTime>(dr, "Created"),
                                Name = GetDataValue<string>(dr, "Name"),
                                Creator = GetDataValue<string>(dr, "Creator")
                            };
                            list.Add(invoice);
                        }
                    }
                }
                return list;
            }
            catch (Exception)
            {
                return null;
            }
        }

        /// <summary>
        /// Returns orders linked to selected invoice run
        /// </summary>
        /// <param name="id">Invoice Run Id</param>
        /// <returns></returns>
        public static Collection<OrderRow> GetInvoiceRunOrders(int id)
        {
            var sqlStatement = Settings.Default.SqlOrderRowColumns + Settings.Default.SqlGetProformaInvoiceRows;
            var db = DatabaseFactory.CreateDatabase(ConnectionName);
            var list = new Collection<OrderRow>();

            using (var cmd = db.GetSqlStringCommand(sqlStatement))
            {
                cmd.CommandTimeout = 600;
                db.AddInParameter(cmd, "@CustomershipId", DbType.Int32, null);
                db.AddInParameter(cmd, "@Id", DbType.Int32, id);
                using (var dr = db.ExecuteReader(cmd))
                {
                    while (dr.Read())
                    {
                        var order = new OrderRow();
                        ReadOrderRow(dr, order);
                        list.Add(order);
                    }
                }
            }

            return list;
        }

        private static Dictionary<string, string> UserExceptions
        {
            get
            {
                var dict = new Dictionary<string, string>
                {
                    {"marjahi", "marja-liisa"}
                };
                return dict;
            }
        }

        public static int GetUserId(string username)
        {
            username = RemoveDomain(username);

            var knownException = UserExceptions.SingleOrDefault(x => x.Key.Equals(username, StringComparison.InvariantCultureIgnoreCase));
            if (!knownException.Equals(default(KeyValuePair<string,string>)))
                username = knownException.Value;

            var sqlStatement = Settings.Default.SqlGetUserId;
            var db = DatabaseFactory.CreateDatabase(ConnectionName);
            int userId;

            using (var cmd = db.GetSqlStringCommand(sqlStatement))
            {
                cmd.CommandTimeout = 600;
                db.AddInParameter(cmd, "@Username", DbType.String, username);
                userId = (int)db.ExecuteScalar(cmd);
            }

            return userId;
        }

        protected static string RemoveDomain(string username)
        {
            return username.Contains(@"\") ? username.Split('\\')[1] : username;
        }

        /// <summary>
        /// Populate order row with data from reader (db)
        /// </summary>
        /// <param name="dr">Reader</param>
        /// <param name="orderRow">Order row to populate</param>
        protected static void ReadOrderRow(IDataReader dr, OrderRow orderRow)
        {
            if (orderRow == null)
                throw new ArgumentNullException("orderRow", "ReadOrderRow - OrderRow can't be null.");

            orderRow.OrderRowId = GetDataValue<string>(dr, "OrderRowId");
            orderRow.OrderId = GetDataValue<string>(dr, "OrderGroupId");
            orderRow.OrderNumber = GetDataValue<string>(dr, "OrderRowNumber");
            orderRow.PriceYear = GetDataValue<short>(dr, "PriceYear");
            orderRow.PriceValidFrom = GetDataValue<DateTime>(dr, "PriceValidFrom");
            orderRow.PriceSource = GetDataValue<string>(dr, "PriceSource");
            orderRow.IsForeign = GetDataValue<bool>(dr, "IsForeign");
            orderRow.Quantity = GetDataValue<short>(dr, "Pcs");
            orderRow.PeriodStart = GetDataValue<DateTime>(dr, "PeriodStart");
            orderRow.PeriodEnd = GetDataValue<DateTime>(dr, "PeriodEnd");
            orderRow.Currency = GetDataValue<string>(dr, "Currency");
            orderRow.Comment1 = GetDataValue<string>(dr, "Comment1");
            orderRow.Comment2 = GetDataValue<string>(dr, "Comment2");
            orderRow.Consolidation = GetDataValue<string>(dr, "Consolidation");
            orderRow.Months = GetDataValue<int>(dr, "Months");
            orderRow.CustomerOrderNumber = GetDataValue<string>(dr, "CustomerOrderNumber");
            orderRow.CostCenter = GetDataValue<string>(dr, "CostCenter");

            orderRow.GrossPrice = GetDataValue<decimal>(dr, "GrossPrice");
            orderRow.DiscountPercent = GetDataValue<decimal>(dr, "DiscountPercent");
            orderRow.Postage = GetDataValue<decimal>(dr, "Postage");
            orderRow.Airmail = GetDataValue<decimal>(dr, "Airmail");
            orderRow.GrossPurchasePrice = GetDataValue<decimal>(dr, "GrossPurchasePrice");
            orderRow.NetPurchasePrice = GetDataValue<decimal>(dr, "NetPurchasePrice");
            orderRow.ManualInvoicing = GetDataValue<bool>(dr, "ManualInvoicing");

            orderRow.Activated = GetDataValue<bool>(dr, "Activated");
            orderRow.AdditionalInvoiced = GetDataValue<bool>(dr, "AdditionalInvoiced");
            orderRow.Canceled = GetDataValue<bool>(dr, "Canceled");
            orderRow.Credited = GetDataValue<bool>(dr, "Credited");
            orderRow.Invoiced = GetDataValue<bool>(dr, "Invoiced");
            orderRow.InvoiceLater = GetDataValue<bool>(dr, "InvoiceLater");
            orderRow.InvoicePending = GetDataValue<bool>(dr, "InvoicePending");
            orderRow.LibNetOrder = GetDataValue<bool>(dr, "LibNetOrder");
            orderRow.NoRenewal = GetDataValue<bool>(dr, "NoRenewal");
            orderRow.OrderSent = GetDataValue<bool>(dr, "OrderSent");
            orderRow.PriceFixed = GetDataValue<bool>(dr, "PriceFixed");
            orderRow.VatPercent = GetDataValue<decimal>(dr, "VatPercent");
            orderRow.Renewed = GetDataValue<bool>(dr, "Renewed");
            orderRow.TransferOrder = GetDataValue<bool>(dr, "TransferOrder");
            orderRow.UnconfirmedRenewal = GetDataValue<bool>(dr, "UnconfirmedRenewal");
            orderRow.OnHold = GetDataValue<bool>(dr, "OnHold");
            orderRow.GroupId = GetDataValue<int>(dr, "GroupId");
            orderRow.MainCustomerId = GetDataValue<string>(dr, "MainCustomerId");
            orderRow.MainCustomer = GetDataValue<string>(dr, "MainCustomer");
            orderRow.CustomerShip = (CustomerShip)GetDataValue<int>(dr, "CustomerShip");
            orderRow.CustomerGroup = GetDataValue<string>(dr, "CustomerGroup");
            orderRow.CustomerClass = GetDataValue<string>(dr, "CustomerClass");
            orderRow.PaymentTerm = GetDataValue<string>(dr, "PaymentTerm");
            orderRow.InvoicingGroupDomestic = GetDataValue<string>(dr, "InvoicingGroupDomestic");
            orderRow.InvoicingGroupForeign = GetDataValue<string>(dr, "InvoicingGroupForeign");
            orderRow.InvoiceCustomerId = GetDataValue<string>(dr, "InvoiceCustomerId");
            orderRow.InvoiceCustomer = GetDataValue<string>(dr, "InvoiceCustomer");
            orderRow.DeliveryCustomerId = GetDataValue<string>(dr, "DeliveryCustomerId");
            orderRow.DeliveryCustomer = GetDataValue<string>(dr, "DeliveryCustomer");
            orderRow.LastInvoiceDate = GetDataValue<DateTime?>(dr, "LastInvoiceDate");
            orderRow.InvoicingDelayedUntil = GetDataValue<DateTime?>(dr, "InvoicingDelayedUntil");
            orderRow.Title.TitleId = GetDataValue<string>(dr, "TitleId");
            orderRow.Title.TitleName = GetDataValue<string>(dr, "TitleName");
            orderRow.Title.TitleCode = GetDataValue<string>(dr, "TitleCode");
            orderRow.Title.Frequency = GetDataValue<int>(dr, "Frequency");
            orderRow.Title.CustomizedPrice = GetDataValue<bool>(dr, "CustomizedPricing");
            orderRow.Title.Category = GetDataValue<string>(dr, "TitleCategory");
            orderRow.Title.Category2 = GetDataValue<string>(dr, "TitleCategory2");
            orderRow.Title.Format = GetDataValue<string>(dr, "TitleFormat");
            orderRow.PublisherName = GetDataValue<string>(dr, "PublisherName");
            orderRow.PurchasePaid = GetDataValue<DateTime?>(dr, "PurchasePaid");
            orderRow.Status = GetDataValue<string>(dr, "Status");
            orderRow.ProductGroupID = GetDataValue<int>(dr, "ProductGroupID");
            orderRow.ProductGroup = GetDataValue<string>(dr, "ProductGroup");
            orderRow.VatDivision = GetDataValue<decimal?>(dr, "VatDivision");
            orderRow.SeriesTitle = GetDataValue<bool>(dr, "SeriesTitle");
            orderRow.CSR = GetDataValue<string>(dr, "CSR");
            orderRow.MainTitleStatus = GetDataValue<string>(dr, "MainTitleStatus");
            orderRow.OrderStatus = GetDataValue<string>(dr, "OrderStatus");
            orderRow.NotViaAgents = GetDataValue<bool>(dr, "NotViaAgents");
            orderRow.CustomerCurrencyRate = GetDataValue<decimal?>(dr, "CustomerCurrencyRate");
            orderRow.OriginalCurrency = GetDataValue<string>(dr, "OriginalCurrency");
            orderRow.NoteText = GetDataValue<string>(dr, "Note");
            orderRow.LMS = GetDataValue<string>(dr, "LMS");
            orderRow.FixedPrices = GetDataValue<bool>(dr, "FixedPrices");
            orderRow.PurchaseInvoiceNumber = GetDataValue<string>(dr, "PurchaseInvoiceNumber");
            orderRow.PaymentMethod = GetDataValue<string>(dr, "PaymentMethod");
            orderRow.StaticOrderNumber = GetDataValue<string>(dr, "StaticOrderNumber");
            orderRow.Medical = GetDataValue<bool>(dr, "Medical");
        }
    }
}
