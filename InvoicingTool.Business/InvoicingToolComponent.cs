﻿using InvoicingTool.Data;
using InvoicingTool.Entities;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace InvoicingTool.Business
{
    public sealed class InvoicingToolComponent : IDisposable
    {
        public static Collection<OrderRow> GetOrderRows(SearchCriterion criterion)
        {
            return DataDAC.SelectOrdersReadyForInvoicing(criterion);
        }

        public static OrderRow GetOrderRow(string orderRowId, CustomerShip customership)
        {
            return DataDAC.SelectOrderRowById(orderRowId, customership);
        }

        public static OrderRow GetOrderRow(OrderRow original, bool onlypurchases = false)
        {
            return DataDAC.SelectOrderRowById(original, onlypurchases);
        }

        public static Collection<ProformaInvoice> GetLatestInvoiceRuns()
        {
            return DataDAC.GetLatestInvoiceRuns();
        }

        public static Boolean UpdateNote(string id, string text)
        {
            return DataDAC.UpdateNote(id, text);
        }

        public static Boolean DeleteProforma(int id)
        {
            return DataDAC.DeleteProforma(id);
        }

        public static Collection<OrderRow> GetInvoiceRunOrders(int invoiceRunId)
        {
            return DataDAC.GetInvoiceRunOrders(invoiceRunId);
        }

        public static ProformaInvoice CreateProformaInvoice(IEnumerable<string> orderRowIds, string name)
        {
            return DataDAC.CreateProformaInvoice(orderRowIds, name);
        }

        public static Collection<CustomerClass> GetCustomerClasses()
        {
            return DataDAC.SelectCustomerClasses();
        }

        public static Collection<CustomerGroup> GetCustomerGroups()
        {
            return DataDAC.SelectCustomerGroups();
        }

        public static Collection<InvoicingGroup> GetInvoicingGroups()
        {
            return DataDAC.SelectInvoicingGroups();
        }

        public static Collection<PaymentTerm> GetPaymentTerms()
        {
            return DataDAC.SelectPaymentTerms();
        }

        public static bool ConfirmOrderRow(OrderRow orderRow, bool invoice)
        {
            return DataDAC.ConfirmOrderRow(orderRow, invoice);
        }

        public void Dispose()
        {
        }
    }
}













